package platform.commons;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Norbert on 4/10/2016.
 */
public final class DataFormatter {
    /**
     * Created by Norbert on 4/13/2016.
     */
    public static <T> DataTableFeed<T> getDataFeed(List<T> result, Integer draw, Integer rowCount) {
        DataTableFeed<T> feed = new DataTableFeed<T>();
        feed.setData(result);
        feed.setDraw(draw);
        feed.setRecordsFiltered(rowCount);
        feed.setRecordsTotal(rowCount);
        return feed;
    }

    public static String encryptPassword(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        crypt.reset();
        crypt.update(password.getBytes("UTF-8"));
        return new BigInteger(1, crypt.digest()).toString(16);
    }

    public static long getRandomUID() {
        return UUID.randomUUID().getMostSignificantBits() % 99999999;
    }
}
