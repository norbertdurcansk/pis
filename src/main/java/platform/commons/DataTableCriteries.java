package platform.commons;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * pis
 * Created by Norbert on 4/3/2017.
 */
public class DataTableCriteries {

    private int draw;
    private int start;
    private int length;
    private Map<String, Object> customFilters;

    private Map<SearchCriterias, String> search;

    private List<Map<ColumnCriterias, String>> columns;

    private List<Map<OrderCriterias, String>> order;

    public enum SearchCriterias {
        value, regex
    }

    public enum OrderCriterias {
        column, dir
    }

    public enum ColumnCriterias {
        data, name, searchable, orderable, searchValue, searchRegex
    }

    public Map<String, Object> getCustomFilters() {
        return customFilters;
    }

    public void setCustomFilters(Map<String, Object> customFilter) {
        this.customFilters = customFilter;
    }

    public void addCustomFilter(String key, Object o) {
        if (customFilters == null) {
            customFilters = new HashMap<String, Object>();
        }
        customFilters.put(key, o);
    }

    public Object getCustomFilter(String key) {
        return customFilters.get(key);
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Map<SearchCriterias, String> getSearch() {
        return search;
    }

    public String getSearchFulltext() {
        return getSearch().get(SearchCriterias.value);
    }

    public String getOrderDirection() {
        return getOrder().get(0).get(OrderCriterias.dir);
    }

    public String getOrderColumn() {
        int orderByColNumIx = Integer.valueOf(getOrder().get(0).get(OrderCriterias.column));
        return getColumns().get(orderByColNumIx).get(ColumnCriterias.data);
    }

    public void setSearch(Map<SearchCriterias, String> search) {
        this.search = search;
    }

    public List<Map<ColumnCriterias, String>> getColumns() {
        return columns;
    }

    public void setColumns(List<Map<ColumnCriterias, String>> columns) {
        this.columns = columns;
    }

    public List<Map<OrderCriterias, String>> getOrder() {
        return order;
    }

    public void setOrder(List<Map<OrderCriterias, String>> order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "DataTableCriteries [draw=" + draw + ", start=" + start + ", length=" + length + ", search=" + search
                + ", columns=" + columns + ", order=" + order + "]";
    }

    public boolean hasSearchField(String columnName) {
        Iterator<Map<ColumnCriterias, String>> itn = columns.iterator();
        while (itn.hasNext()) {
            Map<ColumnCriterias, String> item = itn.next();
            if (item.get(ColumnCriterias.data).equals(columnName)) {
                if (item.get(ColumnCriterias.searchable).equals("true")
                        && item.get(ColumnCriterias.searchValue).length() > 0)
                    return true;
            }
        }
        return false;
    }

    public Object getSearchFieldValue(String columnName) {
        Iterator<Map<ColumnCriterias, String>> itn = columns.iterator();
        while (itn.hasNext()) {
            Map<ColumnCriterias, String> item = itn.next();
            if (item.get(ColumnCriterias.data).equals(columnName)) {
                return item.get(ColumnCriterias.searchValue);
            }
        }
        return false;
    }


}
