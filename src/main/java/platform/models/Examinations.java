package platform.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Entity
@Table(name = "examinations")
public class Examinations {
    private long examinationid;
    private String description;
    private Timestamp date;
    private long userid;
    private long patientid;
    private long departmentid;
    private int deleted;

    /* helper fields */
    @Transient
    String doctor_firstname;
    @Transient
    String doctor_lastname;
    @Transient
    String department_name;
    @Transient
    String patient_firstname;
    @Transient
    String patient_lastname;
    @Transient
    long birthnum;

    @Transient
    public long getBirthnum() {
        return birthnum;
    }

    @Transient
    public void setBirthnum(long birthnum) {
        this.birthnum = birthnum;
    }

    @Transient
    public String getDoctor_firstname() {
        return doctor_firstname;
    }

    @Transient
    public void setDoctor_firstname(String doctor_firstname) {
        this.doctor_firstname = doctor_firstname;
    }

    @Transient
    public String getDoctor_lastname() {
        return doctor_lastname;
    }

    @Transient
    public void setDoctor_lastname(String doctor_lastname) {
        this.doctor_lastname = doctor_lastname;
    }

    @Transient
    public String getDepartment_name() {
        return department_name;
    }

    @Transient
    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    @Transient
    public String getPatient_firstname() {
        return patient_firstname;
    }

    @Transient
    public void setPatient_firstname(String patient_firstname) {
        this.patient_firstname = patient_firstname;
    }

    @Transient
    public String getPatient_lastname() {
        return patient_lastname;
    }

    @Transient
    public void setPatient_lastname(String patient_lastname) {
        this.patient_lastname = patient_lastname;
    }

    @Id
    @Column(name = "examinationid", nullable = false)
    public long getExaminationid() {
        return examinationid;
    }

    public void setExaminationid(long examinationid) {
        this.examinationid = examinationid;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 1023)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "date", nullable = true)
    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Basic
    @Column(name = "userid", nullable = false)
    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    @Basic
    @Column(name = "patientid", nullable = false)
    public long getPatientid() {
        return patientid;
    }

    public void setPatientid(long patientid) {
        this.patientid = patientid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Examinations that = (Examinations) o;

        if (examinationid != that.examinationid) return false;
        if (userid != that.userid) return false;
        if (patientid != that.patientid) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (departmentid != that.departmentid) return false;
        if (deleted != that.deleted) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (examinationid ^ (examinationid >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (int) (userid ^ (userid >>> 32));
        result = 31 * result + (int) (patientid ^ (patientid >>> 32));
        result = 31 * result + (int) (departmentid ^ (departmentid >>> 32));
        result = 31 * result + (deleted ^ (deleted >>> 32));
        return result;
    }

    @Basic
    @Column(name = "departmentid", nullable = false)
    public long getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(long departmentid) {
        this.departmentid = departmentid;
    }
}
