package platform.models;

import javax.persistence.*;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Entity
@Table(name = "departments")
public class Departments {
    private long departmentid;
    private String name;
    private int deleted;

    /* helper fields */
    @Transient
    private String user_firstname;
    @Transient
    private String user_lastname;
    @Transient
    private Long userid;
    @Transient
    private Integer bitflags;
    @Transient
    private String user_email;

    @Transient
    public String getUser_email() {
        return user_email;
    }

    @Transient
    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    @Transient
    public String getUser_firstname() {
        return user_firstname;
    }

    @Transient
    public void setUser_firstname(String user_firstname) {
        this.user_firstname = user_firstname;
    }

    @Transient
    public String getUser_lastname() {
        return user_lastname;
    }

    @Transient
    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    @Transient
    public Long getUserid() {
        return userid;
    }

    @Transient
    public void setUserid(Long userid) {
        this.userid = userid;
    }

    @Transient
    public Integer getBitflags() {
        return bitflags;
    }

    @Transient
    public void setBitflags(Integer bitflags) {
        this.bitflags = bitflags;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }


    @Id
    @Column(name = "departmentid", nullable = false)
    public long getDepartmentid() {
        return departmentid;
    }

    public void setDepartmentid(long departmentid) {
        this.departmentid = departmentid;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 255)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Departments that = (Departments) o;

        if (departmentid != that.departmentid) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (deleted != that.deleted) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (departmentid ^ (departmentid >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + deleted;
        return result;
    }
}
