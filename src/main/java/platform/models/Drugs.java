package platform.models;

import javax.persistence.*;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Entity
@Table(name = "drugs")
public class Drugs {
    private long drugid;
    private String name;
    private String type;
    private String activeSubstance;
    private Integer activeAmount;
    private String producer;
    private int deleted;

    @Basic
    @Column(name = "deleted", nullable = false)
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    /* helper fields */
    @Transient
    Integer pill_amount;
    @Transient
    Long patientdrugid;
    @Transient
    Integer day_amount;

    @Transient
    public Long getPatientdrugid() {
        return patientdrugid;
    }

    @Transient
    public void setPatientdrugid(Long patientdrugid) {
        this.patientdrugid = patientdrugid;
    }

    @Transient
    public Integer getPill_amount() {
        return pill_amount;
    }

    @Transient
    public void setPill_amount(Integer pill_amount) {
        this.pill_amount = pill_amount;
    }

    @Transient
    public Integer getDay_amount() {
        return day_amount;
    }

    @Transient
    public void setDay_amount(Integer day_amount) {
        this.day_amount = day_amount;
    }

    @Id
    @Column(name = "drugid", nullable = false)
    public long getDrugid() {
        return drugid;
    }

    public void setDrugid(long drugid) {
        this.drugid = drugid;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "type", nullable = true, length = 50)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "active_substance", nullable = true, length = 50)
    public String getActiveSubstance() {
        return activeSubstance;
    }

    public void setActiveSubstance(String activeSubstance) {
        this.activeSubstance = activeSubstance;
    }

    @Basic
    @Column(name = "active_amount", nullable = true)
    public Integer getActiveAmount() {
        return activeAmount;
    }

    public void setActiveAmount(Integer activeAmount) {
        this.activeAmount = activeAmount;
    }

    @Basic
    @Column(name = "producer", nullable = true, length = 100)
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Drugs drugs = (Drugs) o;

        if (drugid != drugs.drugid) return false;
        if (name != null ? !name.equals(drugs.name) : drugs.name != null) return false;
        if (type != null ? !type.equals(drugs.type) : drugs.type != null) return false;
        if (activeSubstance != null ? !activeSubstance.equals(drugs.activeSubstance) : drugs.activeSubstance != null)
            return false;
        if (activeAmount != null ? !activeAmount.equals(drugs.activeAmount) : drugs.activeAmount != null) return false;
        if (producer != null ? !producer.equals(drugs.producer) : drugs.producer != null) return false;
        if (deleted != drugs.deleted) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (drugid ^ (drugid >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (activeSubstance != null ? activeSubstance.hashCode() : 0);
        result = 31 * result + (activeAmount != null ? activeAmount.hashCode() : 0);
        result = 31 * result + (producer != null ? producer.hashCode() : 0);
        result = 31 * result + (int) (deleted ^ (deleted >>> 32));
        return result;
    }
}
