package platform.models;

import javax.persistence.*;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Entity
@Table(name = "patient_drug")
public class PatientDrug {
    private long patientid;
    private Integer pillAmount;
    private Integer dayAmount;
    private long drugid;
    private long patientdrugid;
    private int deleted;

    @Basic
    @Column(name = "deleted", nullable = false)
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "patientid", nullable = false)
    public long getPatientid() {
        return patientid;
    }

    public void setPatientid(long patientid) {
        this.patientid = patientid;
    }

    @Basic
    @Column(name = "pill_amount", nullable = true)
    public Integer getPillAmount() {
        return pillAmount;
    }

    public void setPillAmount(Integer pillAmount) {
        this.pillAmount = pillAmount;
    }

    @Basic
    @Column(name = "day_amount", nullable = true)
    public Integer getDayAmount() {
        return dayAmount;
    }

    public void setDayAmount(Integer dayAmount) {
        this.dayAmount = dayAmount;
    }

    @Basic
    @Column(name = "drugid", nullable = false)
    public long getDrugid() {
        return drugid;
    }

    public void setDrugid(long drugid) {
        this.drugid = drugid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PatientDrug that = (PatientDrug) o;

        if (patientid != that.patientid) return false;
        if (drugid != that.drugid) return false;
        if (deleted != that.deleted) return false;
        if (pillAmount != null ? !pillAmount.equals(that.pillAmount) : that.pillAmount != null) return false;
        if (dayAmount != null ? !dayAmount.equals(that.dayAmount) : that.dayAmount != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (patientid ^ (patientid >>> 32));
        result = 31 * result + (pillAmount != null ? pillAmount.hashCode() : 0);
        result = 31 * result + (dayAmount != null ? dayAmount.hashCode() : 0);
        result = 31 * result + (int) (drugid ^ (drugid >>> 32));
        result = 31 * result + deleted;
        return result;
    }

    @Id
    @Column(name = "patientdrugid", nullable = false)
    public long getPatientdrugid() {
        return patientdrugid;
    }

    public void setPatientdrugid(long patientdrugid) {
        this.patientdrugid = patientdrugid;
    }
}
