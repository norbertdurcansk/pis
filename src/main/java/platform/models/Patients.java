package platform.models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Entity
@Table(name = "patients")
public class Patients {
    private long patientid;
    private long birthnum;
    private String firstname;
    private String lastname;
    private Timestamp birthdate;
    private String sex;
    private String city;
    private String street;
    private String postalcode;
    private String phone;
    private int deleted;

    @Basic
    @Column(name = "deleted", nullable = false)
    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    @Id
    @Column(name = "patientid", nullable = false)
    public long getPatientid() {
        return patientid;
    }

    public void setPatientid(long patientid) {
        this.patientid = patientid;
    }

    @Basic
    @Column(name = "birthnum", nullable = false)
    public long getBirthnum() {
        return birthnum;
    }

    public void setBirthnum(long birthnum) {
        this.birthnum = birthnum;
    }

    @Basic
    @Column(name = "firstname", nullable = false, length = 50)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "lastname", nullable = false, length = 50)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "birthdate", nullable = false)
    public Timestamp getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Timestamp birthdate) {
        this.birthdate = birthdate;
    }

    @Basic
    @Column(name = "sex", nullable = false, length = 10)
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 50)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "street", nullable = true, length = 50)
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    @Basic
    @Column(name = "postalcode", nullable = true, length = 10)
    public String getPostalcode() {
        return postalcode;
    }

    public void setPostalcode(String postalcode) {
        this.postalcode = postalcode;
    }

    @Basic
    @Column(name = "phone", nullable = true, length = 50)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Patients patients = (Patients) o;

        if (patientid != patients.patientid) return false;
        if (birthnum != patients.birthnum) return false;
        if (firstname != null ? !firstname.equals(patients.firstname) : patients.firstname != null) return false;
        if (lastname != null ? !lastname.equals(patients.lastname) : patients.lastname != null) return false;
        if (birthdate != null ? !birthdate.equals(patients.birthdate) : patients.birthdate != null) return false;
        if (sex != null ? !sex.equals(patients.sex) : patients.sex != null) return false;
        if (city != null ? !city.equals(patients.city) : patients.city != null) return false;
        if (street != null ? !street.equals(patients.street) : patients.street != null) return false;
        if (postalcode != null ? !postalcode.equals(patients.postalcode) : patients.postalcode != null) return false;
        if (phone != null ? !phone.equals(patients.phone) : patients.phone != null) return false;
        if (deleted != patients.deleted) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (patientid ^ (patientid >>> 32));
        result = 31 * result + (int) (birthnum ^ (birthnum >>> 32));
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (birthdate != null ? birthdate.hashCode() : 0);
        result = 31 * result + (sex != null ? sex.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (postalcode != null ? postalcode.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (int) (deleted ^ (deleted >>> 32));
        return result;
    }
}
