package platform.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by norbe on 3/6/2017.
 */

@Controller
public class LoginController {

    @Autowired
    IUserService userService;

    @RequestMapping(value = "/login-page" , method = RequestMethod.GET)
    public String login(Model model,HttpServletRequest request) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && !auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ANONYMOUS"))){
            return "redirect:/home";
        }
        if(request.getParameter("error")!=null) {
            model.addAttribute("error", true);
        }
        return "/login-page";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String rootPage(HttpServletRequest request, HttpServletResponse response) {
        return "redirect:/home";
    }

    @RequestMapping(value = "/logout" , method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login-page?logout";
    }
}
