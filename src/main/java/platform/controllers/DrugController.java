package platform.controllers;

import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;
import platform.models.Drugs;
import platform.models.Patients;
import platform.models.Users;
import platform.services.interfaces.IDepartmentService;
import platform.services.interfaces.IDrugService;
import platform.services.interfaces.IPatientService;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Jusko on 01-May-17.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_USER')")
@RequestMapping(value = "/drugs")
public class DrugController {

    @Autowired
    IUserService userService;

    @Autowired
    IDepartmentService departmentService;

    @Autowired
    IPatientService patientService;

    @Autowired
    IDrugService drugService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String patientsPage(Model model, HttpServletRequest request) {


        return "drugs/drugs";
    }

    @RequestMapping(value = "/all/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Drugs> loadDrugs(DataTableCriteries criteries, HttpServletRequest request) {
        return drugService.findAllForCriteries(criteries);
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String newDrug(Model model) {
        Drugs drug = new Drugs();
        drug.setDrugid(-1);

        model.addAttribute("drug", drug);
        return "drugs/create";
    }

    @RequestMapping(value = "/{drugid}/update", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject update(HttpServletRequest request, @PathVariable("drugid") Long drugid, @ModelAttribute("drug") Drugs drug) {
        JSONObject jsonObject = new JSONObject();
        try {
            drugService.finalize(drug);
        }
        catch (Exception e) {
            jsonObject.put("error", "Uživatel nenalezen.");
            return jsonObject;
        }

        drugService.save(drug);

        jsonObject.put("drug", drug);

        return jsonObject;
    }


    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editPatient(Model model, HttpServletRequest request) {

        Drugs drug;

        if (request.getParameter("drugid") != null) {
            drug = drugService.findOne(Long.valueOf(request.getParameter("drugid")));
        } else {
            drug = new Drugs();
        }

        model.addAttribute("drug", drug);
        return "drugs/create";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deletePatient(Model model, HttpServletRequest request) {

        Drugs drug;

        if (request.getParameter("drugid") != null) {
            drug = drugService.findOne(Long.valueOf(request.getParameter("drugid")));

        } else {
            drug = new Drugs();
        }

        drug.setDeleted(1);
        drugService.save(drug);

        return "drugs/drugs";
    }
}
