package platform.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Examinations;
import platform.models.Users;
import platform.services.interfaces.IDepartmentService;
import platform.services.interfaces.IExaminationService;
import platform.services.interfaces.IPatientService;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * pis
 * Created by Norbert on 5/1/2017.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_USER')")
@RequestMapping("/examination")
public class ExaminationController {


    @Autowired
    IExaminationService examinationService;

    @Autowired
    IPatientService patientService;

    @Autowired
    IDepartmentService departmentService;

    @Autowired
    IUserService userService;


    @RequestMapping("/home")
    public String homePage(Model model) {
        model.addAttribute("isdoc", BaseController.hasPermission("3"));
        model.addAttribute("isadmin", BaseController.hasPermission("7"));
        return "examination/examination";
    }

    @RequestMapping(value = "/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Examinations> loadExaminations(DataTableCriteries criteries, HttpServletRequest request) {
        Users user = userService.findOne(BaseController.getUserId());
        return examinationService.findAllForCriteries(criteries, user.getUserid(), user.getDepartmentid());
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    public String edit(HttpServletRequest request, Examinations examinations) {
        examinationService.finalize(examinations);
        examinationService.save(examinations);
        return "OK - 200";
    }

    @RequestMapping(value = "/{examinationid}/delete", method = RequestMethod.GET)
    public String update(HttpServletRequest request, @PathVariable("examinationid") Long examinationid) {
        Examinations examinations = examinationService.findOne(examinationid);
        examinations.setDeleted(1);
        examinationService.save(examinations);
        return "redirect:/examination/home";
    }

    @RequestMapping(value = "{examinationid}/form", method = RequestMethod.GET)
    public String editForm(Model model, @PathVariable("examinationid") Long examinationid) {
        Examinations examinations;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        if (examinationid != -1 && BaseController.hasPermission("3")) {
            examinations = examinationService.findOne(examinationid);
            String news = sdf.format(new Date(examinations.getDate().getTime()));
            model.addAttribute("date", news);
        } else {
            examinations = new Examinations();
            examinations.setExaminationid(-1);
            examinations.setDate(new Timestamp(new Date().getTime()));
            String news = sdf.format(new Date());
            model.addAttribute("date", news);
        }
        model.addAttribute("examination", examinations);
        model.addAttribute("patients", patientService.getPatients());
        return "examination/examination-edit";
    }


}
