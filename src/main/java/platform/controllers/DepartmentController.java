package platform.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;
import platform.services.interfaces.IDepartmentService;

import javax.servlet.http.HttpServletRequest;

@Controller
@PreAuthorize("hasAnyRole('ROLE_USER')")
@RequestMapping(value = "/departments")
public class DepartmentController {

    @Autowired
    IDepartmentService departService;

    @RequestMapping("/home")
    public String departments (Model model) {

        model.addAttribute("isAdmin", BaseController.hasPermission("7"));

        return "/departments/departments";
    }

    @RequestMapping(value = "/home/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Departments> loadDepartments(DataTableCriteries criteries, HttpServletRequest request) {
        return departService.findAllForCriteries(criteries);
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public String departmentDetail (Model model, HttpServletRequest req) {
        long id;
        try {
            id = Long.parseLong(req.getParameter("id"));
        }
        catch (Exception e) {
            return "/home";
        }

        Departments department = departService.findOne(id);
        model.addAttribute("department_name", department.getName());
        model.addAttribute("departmentid", id); //Kvuli nacitani dat do tabulky

        return "/departments/department-detail";
    }

    @RequestMapping(value = "/detail/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Departments> loadDepartmentDetail(DataTableCriteries criteries, HttpServletRequest request) {
        Long id = Long.valueOf(request.getParameter("id"));

        return departService.findAllForDetail(criteries, id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addPage(Model model, HttpServletRequest req) {
        long id = Long.parseLong(req.getParameter("id"));
        Departments department;
        if (id == -1)
            department = new Departments();
        else
            department = departService.findOne(id);

        model.addAttribute("department", department);
        model.addAttribute("id_depart", id);

        return "/departments/department-new";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/add_department", method = RequestMethod.POST)
    @ResponseBody
    public String addDepartment(Model model, HttpServletRequest req) throws Exception {
        long id;
        String departmentName;
        Departments department = new Departments();

        try {id = Long.parseLong(req.getParameter("id"));}
        catch (Exception e){return "error";}

        departmentName = req.getParameter("name");
        if (departmentName == null)
            return "error";

        department.setDepartmentid(id);

        departService.finalize(department);
        department.setName(departmentName);

        departService.save(department);

        return "OK-200";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteDepartment (Model model, HttpServletRequest req) {
        long id;
        try{id = Long.parseLong(req.getParameter("id"));}
        catch (Exception e){return "/home"; }

        Departments department = departService.findOne(id);
        department.setDeleted(1);
        departService.save(department);

        model.addAttribute("isAdmin", BaseController.hasPermission("7")); //Pro zobrazeni tlacitek

        return "/departments/departments";
    }




}
