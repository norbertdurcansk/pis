package platform.controllers;

import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;
import platform.models.Users;
import platform.services.interfaces.IDepartmentService;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
//import platform.models.User;


/**
 * Created by norbe on 3/7/2017.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_USER')")
@RequestMapping(value = "/setting")
public class SettingController {

    @Autowired
    IUserService userService;

    @Autowired
    IDepartmentService departmentService;

    @ModelAttribute("users")
    public Users request(HttpSession session, HttpServletRequest req) throws Exception {
        Long pathvar = null;
        try {
            String prefix[] = req.getServletPath().split("/update")[0].split("/");
            pathvar = Long.valueOf(prefix[prefix.length - 1]);
        } catch (Exception e) {
        }
        if (pathvar == null) {
            return null;
        }
        if (pathvar.equals(-1L)) {
            Users user = new Users();
            return user;
        } else {
            return userService.findOne(Long.valueOf(pathvar));
        }
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String settingPage(Model model, HttpServletRequest request) {

        Users user;
        if (request.getParameter("userid") != null && request.isUserInRole("ROLE_ADMIN")) {
            user = userService.findOne(Long.valueOf(request.getParameter("userid")));
        } else {
            user = userService.findOne(BaseController.getUserId());
        }
        Departments department = departmentService.findOne(user.getDepartmentid());
        List<Departments> departments = departmentService.getList();

        model.addAttribute("departments", departments);
        model.addAttribute("user", user);
        model.addAttribute("userid", BaseController.getUserId());
        model.addAttribute("department", department);
        model.addAttribute("newUser", false);
        return "setting/setting";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/{userid}/delete", method = RequestMethod.GET)
    public String deleteUser(Model model, HttpServletRequest request, @PathVariable("userid") Long userid) {
        Users users = userService.findOne(userid);
        users.setDeleted(1);
        userService.save(users);
        return "redirect:/setting/home";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/user/new", method = RequestMethod.GET)
    public String newUser(Model model) {
        Users user = new Users();
        user.setUserid(-1);
        Departments department = new Departments();
        List<Departments> departments = departmentService.getList();
        model.addAttribute("departments", departments);
        model.addAttribute("user", user);
        model.addAttribute("newUser", true);
        return "setting/setting";
    }

    @RequestMapping(value = "/user/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Users> loadUsers(DataTableCriteries criteries, HttpServletRequest request) {
        return userService.findAllForCriteries(criteries);
    }


    @RequestMapping(value = "/{userid}/update", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject update(HttpServletRequest request, @PathVariable("userid") Long userid, @ModelAttribute("users") Users user) {
        String passwd = null;
        Departments department;
        JSONObject jsonObject = new JSONObject();
        try {
            if (userid == -1) {
                if (userService.findOneByEmail(user.getEmail()) != null) {
                    jsonObject.put("error", "Uživatel již existuje.");
                    return jsonObject;
                }
            }
            department = departmentService.findOne(user.getDepartmentid());

            String newP1 = (request.getParameter("newpasswd") != null) ? request.getParameter("newpasswd") : "";
            String newP2 = (request.getParameter("newpasswd1") != null) ? request.getParameter("newpasswd1") : "";
            String oldP = (request.getParameter("oldpasswd") != null) ? request.getParameter("oldpasswd") : "";

            if (!newP1.equals("") && !newP2.equals("")) {

                if (((BaseController.isAdmin() && user.getUserid() != BaseController.getUserId()) || (DataFormatter.encryptPassword(oldP).equals(user.getPassword()))) && newP1.equals(newP2)) {
                    passwd = newP2;
                } else {
                    jsonObject.put("error", "Chybne zadané heslo.");
                    return jsonObject;
                }
            }
            userService.finalize(user, passwd);
        } catch (Exception e) {
            jsonObject.put("error", "Uživatel nenalezen.");
            return jsonObject;
        }
        userService.save(user);
        jsonObject.put("user", user);
        jsonObject.put("department", department);
        return jsonObject;
    }
}
