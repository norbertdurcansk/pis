package platform.controllers;

import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.*;
import platform.services.interfaces.IDepartmentService;
import platform.services.interfaces.IDrugService;
import platform.services.interfaces.IPatientService;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
//import platform.models.User;

/**
 * Created by Jusko on 01-May-17.
 */
@Controller
@PreAuthorize("hasAnyRole('ROLE_USER')")
@RequestMapping(value = "/patient")
public class PatientsController {

    @Autowired
    IUserService userService;

    @Autowired
    IDepartmentService departmentService;

    @Autowired
    IPatientService patientService;

    @Autowired
    IDrugService drugService;



    @RequestMapping(value = "/all/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Patients> loadPatients(DataTableCriteries criteries, HttpServletRequest request) {
        return patientService.findAllForCriteries(criteries);
    }

    @RequestMapping(value = "/{id}/drugs/load", method = RequestMethod.GET)
    @ResponseBody
    public DataTableFeed<Drugs> loadDrugs(DataTableCriteries criteries, HttpServletRequest request, @PathVariable("id") Long id) {
        return drugService.findAllForPatient(criteries, id);
    }

    @RequestMapping(value = "/{patientid}/drugs/{drugid}/form", method = RequestMethod.GET)
    public String formPage(Model model, HttpServletRequest request, @PathVariable("patientid") Long patientid, @PathVariable("drugid") Long drugid) {
        PatientDrug drug;
        if (drugid == -1L) {
            drug = new PatientDrug();
            drug.setPatientid(patientid);
            drug.setPatientdrugid(-1L);
        } else {
            drug = drugService.findOneDrug(drugid);
        }
        model.addAttribute("patientdrug", drug);
        model.addAttribute("drugs", drugService.getDrugs());
        return "patient/patient-drug-edit";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String patientsPage(Model model, HttpServletRequest request) {

        Users user;
        if (request.getParameter("userid") != null && request.isUserInRole("ROLE_ADMIN")) {
            user = userService.findOne(Long.valueOf(request.getParameter("userid")));
        } else {
            user = userService.findOne(BaseController.getUserId());
        }
        Departments department = departmentService.findOne(user.getDepartmentid());
        List<Departments> departments = departmentService.getList();

        model.addAttribute("departments", departments);
        model.addAttribute("user", user);
        model.addAttribute("userid", BaseController.getUserId());
        model.addAttribute("department", department);
        model.addAttribute("newUser", false);
        return "patient/patient";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String newPatient(Model model) {
        Patients patient = new Patients();
        patient.setPatientid(-1);
        patient.setBirthdate(new Timestamp(new Date().getTime()));

        model.addAttribute("patient", patient);

        return "patient/create";
    }

    @RequestMapping(value = "/{patientid}/update", method = RequestMethod.POST)
    @ResponseBody
    public JSONObject update(HttpServletRequest request, @PathVariable("patientid") Long patientid, Patients patient) {
        JSONObject jsonObject = new JSONObject();
        try {
            if (patientid == -1) {
                if (patientService.findOneByBirthNum(patient.getBirthnum()) != null) {
                    jsonObject.put("error", "Pacient již existuje.");
                    return jsonObject;
                }
            }

            patientService.finalize(patient);
        } catch (Exception e) {
            jsonObject.put("error", "Uživatel nenalezen.");
            return jsonObject;
        }

        patientService.save(patient);

        jsonObject.put("patient", patient);
        return jsonObject;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editPatient(Model model, HttpServletRequest request) {

        Patients patient;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date;
        if (request.getParameter("patientid") != null) {
            patient = patientService.findOne(Long.valueOf(request.getParameter("patientid")));
            date = sdf.format(new Date(patient.getBirthdate().getTime()));
        } else {
            patient = new Patients();
            patient.setBirthdate(new Timestamp(new Date().getTime()));
            date = sdf.format(new Date());
        }
        model.addAttribute("bday", date);
        model.addAttribute("patient", patient);

        return "patient/create";
    }


    @RequestMapping(value = "/drugs/update", method = RequestMethod.POST)
    @ResponseBody
    public String updateDrugs(Model model, HttpServletRequest request, PatientDrug patientDrug) {
        drugService.finalize(patientDrug);
        drugService.save(patientDrug);
        return "OK-200";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deletePatient(Model model, HttpServletRequest request) {

        Patients patient;

        if (request.getParameter("patientid") != null) {
            patient = patientService.findOne(Long.valueOf(request.getParameter("patientid")));

        } else {
            patient = new Patients();
        }

        patient.setDeleted(1);
        patientService.save(patient);

        return "patient/patient";
    }

    @RequestMapping(value = "/drugs/{drugid}/delete", method = RequestMethod.GET)
    public String deleteDrug(Model model, HttpServletRequest request, @PathVariable("drugid") Long drugid) {
        PatientDrug patientDrug = drugService.findOneDrug(drugid);
        patientDrug.setDeleted(1);
        drugService.save(patientDrug);
        return "redirect:/patient/show?patientid=" + String.valueOf(patientDrug.getPatientid());
    }

    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public String showPatient(Model model, HttpServletRequest request) {

        Patients patient;
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
        String date;

        if (request.getParameter("patientid") != null) {
            patient = patientService.findOne(Long.valueOf(request.getParameter("patientid")));
            date = sdf.format(new Date(patient.getBirthdate().getTime()));
        } else {
            patient = new Patients();
            date = sdf.format(new Date());
        }
        model.addAttribute("bday", date);
        model.addAttribute("patient", patient);
        model.addAttribute("isDoc", BaseController.hasPermission("3"));
        return "patient/show";
    }

}
