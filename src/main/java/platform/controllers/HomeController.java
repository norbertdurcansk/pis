package platform.controllers;


import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import platform.models.Users;
import platform.services.interfaces.IDrugService;
import platform.services.interfaces.IExaminationService;
import platform.services.interfaces.IPatientService;
import platform.services.interfaces.IUserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
public class HomeController {

    @Autowired
    IUserService userService;

    @Autowired
    IPatientService patientService;

    @Autowired
    IExaminationService examinationService;

    @Autowired
    IDrugService drugService;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homePage(Model model, HttpServletRequest request) {

        Users user;

        user = userService.findOne(BaseController.getUserId());
        model.addAttribute("user", user);
        model.addAttribute("examCount", examinationService.countLastMonth());

        int count = patientService.getNumofPatients();
        model.addAttribute("count", count);

        List<Object> drugProducers = drugService.findDrugProducers();
        final JSONArray array = new JSONArray();
        for (Object object : drugProducers) {
            Object[] in = (Object[]) object;
            if (in[0] == null || in[0].toString().equals("")) continue;
            JSONObject json = new JSONObject();
            json.put("label", in[0]);
            json.put("value", in[1]);
            array.add(json);
        }
        model.addAttribute("graphData", array.toString());

        return "home";
    }


}