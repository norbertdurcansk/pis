package platform.controllers;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;


/**
 * Created by norbe on 3/2/2017.
 */

public class BaseController extends HandlerInterceptorAdapter {

    private static final Logger log = Logger.getLogger(BaseController.class.getName());

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info(request.getRequestURI());
        return true;
    }

    public static Long getUserId(){
        return Long.valueOf(SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[SecurityContextHolder.getContext().getAuthentication().getAuthorities().size() - 1].toString());
    }

    public static Boolean isAdmin() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }

    public static Boolean hasPermission(String permission) {
        String perm = SecurityContextHolder.getContext().getAuthentication().getAuthorities().toArray()[SecurityContextHolder.getContext().getAuthentication().getAuthorities().size() - 2].toString();
        if ((Integer.valueOf(perm.substring(perm.length() - 1)) & Integer.valueOf(permission)) == Integer.valueOf(permission)) {
            return true;
        }
        return false;
    }

    public static Boolean isUser() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority("ROLE_USER"));
    }

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
