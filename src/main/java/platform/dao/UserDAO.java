package platform.dao;

import platform.models.Users;
import platform.services.interfaces.IDataTables;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
public interface UserDAO extends IDataTables<Users> {
    void save(Users user);


    List<Users> getUsers();

    Users findOneByEmail(String email);

    Users findOne(Long id);
}
