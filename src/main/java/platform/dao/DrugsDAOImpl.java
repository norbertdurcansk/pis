package platform.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Drugs;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Repository
public class DrugsDAOImpl implements DrugsDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    public DataTableFeed<Drugs> findAllForCriteries(DataTableCriteries criteries) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        Criteria criteria = session.createCriteria(Drugs.class);
        criteria.add(Restrictions.eq("deleted", 0));

        if (search != null && search != "") {
            criteria.add(Restrictions.or(Restrictions.or(Restrictions.ilike("firstname", search + "%"), Restrictions.ilike("lastname", search + "%")),
                    Restrictions.ilike("email", search + "%")
            ));
        }
        if ("asc".equals(criteries.getOrderDirection())) {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).asc());
        } else {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).desc());
        }
//        get row count not filtered
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        criteria.setProjection(null);
        criteria.setFirstResult(criteries.getStart());
        criteria.setMaxResults(criteries.getLength());

        List<Drugs> patients = criteria.list();
        session.close();

        return DataFormatter.getDataFeed(patients, criteries.getDraw(), count.intValue());
    }

    public List<Drugs> getDrugs() {
        Session session = this.sessionFactory.openSession();
        List<Drugs> drugsList = session.createQuery("from Drugs  where deleted=0").list();
        session.close();
        return drugsList;
    }


    @Transactional
    public DataTableFeed<Drugs> findAllForPatient(DataTableCriteries criteries, Long id) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        String sql;
        sql = "SELECT drugs.drugid,patient_drug.patientdrugid as patientdrugid, drugs.active_substance as activeSubstance,drugs.producer,drugs.type,drugs.name, patient_drug.day_amount day_amount,patient_drug.pill_amount as pill_amount from" +
                " drugs INNER join patient_drug on patient_drug.patientid=" + id + " and patient_drug.drugid=drugs.drugid WHERE patient_drug.deleted=0 ";

        if (search != null && search != "") {
            sql += " and (drugs.name like \'" + search + "%\'";
            sql += "or drugs.type like \'" + search + "%\'";
            sql += "or drugs.active_substance like \'" + search + "%\'";
            sql += "or drugs.producer like \'" + search + "%\') ";
        }
        if ("asc".equals(criteries.getOrderDirection())) {
            sql += " order by " + criteries.getOrderColumn() + " asc";
        } else {
            sql += " order by " + criteries.getOrderColumn() + " desc";
        }

        Integer count = session.createSQLQuery(sql)
                .addScalar("drugid", StandardBasicTypes.LONG)
                .setResultTransformer(Transformers.aliasToBean(Drugs.class)).list().size();

        List<Drugs> items = session.createSQLQuery(sql)
                .addScalar("drugid", StandardBasicTypes.LONG)
                .addScalar("patientdrugid", StandardBasicTypes.LONG)
                .addScalar("activeSubstance", StandardBasicTypes.STRING)
                .addScalar("producer", StandardBasicTypes.STRING)
                .addScalar("type", StandardBasicTypes.STRING)
                .addScalar("name", StandardBasicTypes.STRING)
                .addScalar("pill_amount", StandardBasicTypes.INTEGER)
                .addScalar("day_amount", StandardBasicTypes.INTEGER)
                .setFirstResult(criteries.getStart())
                .setMaxResults(criteries.getLength())
                .setResultTransformer(Transformers.aliasToBean(Drugs.class)).list();
        session.close();
        return DataFormatter.getDataFeed(items, criteries.getDraw(), count);
    }



    public void save(Drugs drug) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(drug);
        session.getTransaction().commit();
        session.close();
    }

    public Drugs findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Drugs where drugid=:id");
        query.setParameter("id", id);
        Drugs drug = (Drugs) query.uniqueResult();
        session.close();
        return drug;
    }

    public List<Object> findDrugProducers()
    {
        Session session = this.sessionFactory.openSession();
        String sql = "SELECT producer as prod, COUNT(*) as count FROM Drugs WHERE deleted = 0 GROUP BY producer";
        session.createQuery(sql);
        List<Object> result = session.createSQLQuery(sql).list();
        session.close();
        return result;
    }
}
