package platform.dao;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;
import platform.services.interfaces.IDataTables;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
public interface DepartmentDAO extends IDataTables<Departments> {

    void save(Departments department);

    Departments findOne(Long id);

    DataTableFeed<Departments> findAllForDetail(DataTableCriteries criteries, Long id);
    List<Departments> getList();
}
