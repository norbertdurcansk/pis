package platform.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.controllers.BaseController;
import platform.models.Examinations;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Repository
public class ExaminationDAOImpl implements ExaminationDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries) {
        return null;
    }

    @Transactional
    public DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries, Long userid, Long departmentid) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        String sql;
        sql = "SELECT description,examinations.userid,examinations.patientid,examinations.departmentid,examinationid,date,patients.birthnum,patients.firstname as patient_firstname,patients.lastname as patient_lastname,departments.name as department_name," +
                "users.firstname as doctor_firstname,users.lastname as doctor_lastname from " +
                "examinations left outer join departments on departments.departmentid=examinations.departmentid left outer join patients on patients.patientid=examinations.patientid " +
                "left outer join users on users.userid=examinations.userid where examinations.deleted=0";


        if (!BaseController.isAdmin() && BaseController.hasPermission("3")) {
            sql += " and examinations.userid=" + String.valueOf(userid);
        } else if (!BaseController.isAdmin() && !BaseController.hasPermission("3") && BaseController.hasPermission("1")) {
            sql += " and examinations.departmentid=" + String.valueOf(departmentid);
        }

        if (search != null && search != "") {
            sql += " and (patients.firstname like \'" + search + "%\'";
            sql += "or patients.lastname like \'" + search + "%\'";
            sql += "or departments.name like \'" + search + "%\'";
            sql += "or users.lastname like \'" + search + "%\'";
            sql += "or users.firstname like \'" + search + "%\')";
        }

        if ("asc".equals(criteries.getOrderDirection())) {
            sql += " order by " + criteries.getOrderColumn() + " asc";
        } else {
            sql += " order by " + criteries.getOrderColumn() + " desc";
        }

        Integer count = session.createSQLQuery(sql)
                .addScalar("examinationid", StandardBasicTypes.LONG)
                .setResultTransformer(Transformers.aliasToBean(Examinations.class)).list().size();

        List<Examinations> items = session.createSQLQuery(sql)
                .addScalar("date")
                .addScalar("birthnum", StandardBasicTypes.LONG)
                .addScalar("description", StandardBasicTypes.STRING)
                .addScalar("patient_firstname", StandardBasicTypes.STRING)
                .addScalar("patient_lastname", StandardBasicTypes.STRING)
                .addScalar("doctor_firstname", StandardBasicTypes.STRING)
                .addScalar("doctor_lastname", StandardBasicTypes.STRING)
                .addScalar("department_name", StandardBasicTypes.STRING)
                .addScalar("examinationid", StandardBasicTypes.LONG)
                .addScalar("departmentid", StandardBasicTypes.LONG)
                .addScalar("userid", StandardBasicTypes.LONG)
                .addScalar("patientid", StandardBasicTypes.LONG)
                .setFirstResult(criteries.getStart())
                .setMaxResults(criteries.getLength())
                .setResultTransformer(Transformers.aliasToBean(Examinations.class)).list();

        session.close();

        return DataFormatter.getDataFeed(items, criteries.getDraw(), count);
    }

    public void save(Examinations examinations) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(examinations);
        session.getTransaction().commit();
        session.close();
    }

    public Integer countLastMonth() {
        Calendar calStart = Calendar.getInstance();
        calStart.setTime(new Date());
        calStart.add(Calendar.MONTH, -1);
        SimpleDateFormat frmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Session session = this.sessionFactory.openSession();
        Integer count = session.createCriteria(Examinations.class)
                .add(Restrictions.sqlRestriction("date between " + " '" + frmt.format(calStart.getTime()) + "' " + " and  '" + frmt.format(new Date()) + "' and deleted=0")).list()
                .size();
        session.close();
        return count;
    }


    public Examinations findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Examinations where examinationid=:id");
        query.setParameter("id", id);
        Examinations examinations = (Examinations) query.uniqueResult();
        session.close();
        return examinations;
    }

}
