package platform.dao;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Drugs;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */

public interface DrugsDAO {

    DataTableFeed<Drugs> findAllForCriteries(DataTableCriteries criteries);

    DataTableFeed<Drugs> findAllForPatient(DataTableCriteries criteries, Long id);
    void save(Drugs drug);

    List<Drugs> getDrugs();
    Drugs findOne(Long id);

    List<Object> findDrugProducers();
}
