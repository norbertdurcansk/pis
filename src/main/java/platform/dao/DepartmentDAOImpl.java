package platform.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.transaction.annotation.Transactional;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
public class DepartmentDAOImpl implements DepartmentDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Departments findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Departments where departmentid=:id");
        query.setParameter("id", id);
        Departments department = (Departments) query.uniqueResult();
        session.close();
        return department;
    }

    public List<Departments> getList() {
        Session session = this.sessionFactory.openSession();
        List<Departments> departmentsList = session.createQuery("from Departments where deleted=0").list();
        session.close();
        return departmentsList;

    }

    @Transactional
    public DataTableFeed<Departments> findAllForDetail(DataTableCriteries criteries, Long id) {


        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        String sql;
        sql = "SELECT departments.departmentid,users.email as user_email,departments.name,users.userid as userid, users.firstname as user_firstname, users.lastname as user_lastname, "
                + "users.bitflags from departments inner join users on departments.departmentid=users.departmentid where departments.deleted=0 and departments.departmentid=" + id + " ";

        if (search != null && search != "") {
            sql += "and( users.firstname like \'" + search + "%\' ";
            sql += "or users.lastname like \'" + search + "%\' ";
            sql += "or name like \'" + search + "%\' ";
            sql += " or users.email like \'" + search + "%\')";
        }

        if ("asc".equals(criteries.getOrderDirection())) {
            sql += " order by " + criteries.getOrderColumn() + " asc";
        } else {
            sql += " order by " + criteries.getOrderColumn() + " desc";
        }

        Integer count = session.createSQLQuery(sql)
                .addScalar("departmentid", StandardBasicTypes.LONG)
                .setResultTransformer(Transformers.aliasToBean(Departments.class)).list().size();

        List<Departments> items = session.createSQLQuery(sql)
                .addScalar("departmentid", StandardBasicTypes.LONG)
                .addScalar("userid", StandardBasicTypes.LONG)
                .addScalar("user_firstname", StandardBasicTypes.STRING)
                .addScalar("user_lastname", StandardBasicTypes.STRING)
                .addScalar("name", StandardBasicTypes.STRING)
                .addScalar("user_email", StandardBasicTypes.STRING)
                .addScalar("bitflags", StandardBasicTypes.INTEGER)
                .setFirstResult(criteries.getStart())
                .setMaxResults(criteries.getLength())
                .setResultTransformer(Transformers.aliasToBean(Departments.class)).list();
        session.close();
        return DataFormatter.getDataFeed(items, criteries.getDraw(), count);
    }

    @Transactional
    public DataTableFeed<Departments> findAllForCriteries(DataTableCriteries criteries) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        Criteria criteria = session.createCriteria(Departments.class);
        criteria.add(Restrictions.eq("deleted", 0));
        if (search != null && search != "") {
            criteria.add(Restrictions.ilike("name", search + "%")
            );
        }
        if ("asc".equals(criteries.getOrderDirection())) {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).asc());
        } else {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).desc());
        }
        // get row count not filtered
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        criteria.setProjection(null);
        criteria.setFirstResult(criteries.getStart());
        criteria.setMaxResults(criteries.getLength());

        List<Departments> departments = criteria.list();
        session.close();

        return DataFormatter.getDataFeed(departments, criteries.getDraw(), count.intValue());
    }

    public void save(Departments department) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(department);
        session.getTransaction().commit();
        session.close();
    }
}
