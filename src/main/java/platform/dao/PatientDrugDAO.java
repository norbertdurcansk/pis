package platform.dao;

import platform.models.PatientDrug;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface PatientDrugDAO {
    PatientDrug findOne(Long id);

    void save(PatientDrug drug);
}
