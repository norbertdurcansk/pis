package platform.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import platform.models.PatientDrug;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */

@Repository
public class PatientDrugDAOImpl implements PatientDrugDAO {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public PatientDrug findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from PatientDrug  where patientdrugid=:id");
        query.setParameter("id", id);
        PatientDrug patientDrug = (PatientDrug) query.uniqueResult();
        session.close();
        return patientDrug;
    }

    public void save(PatientDrug drug) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(drug);
        session.getTransaction().commit();
        session.close();
    }

}
