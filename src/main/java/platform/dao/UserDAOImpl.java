package platform.dao;


import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Users;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
@Repository
public class UserDAOImpl implements UserDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Users user) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(user);
        session.getTransaction().commit();
        session.close();
    }

    @Transactional
    public DataTableFeed<Users> findAllForCriteries(DataTableCriteries criteries) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        Criteria criteria = session.createCriteria(Users.class);
        criteria.add(Restrictions.eq("deleted", 0));
        if (search != null && search != "") {
            criteria.add(Restrictions.or(Restrictions.or(Restrictions.ilike("firstname", search + "%"), Restrictions.ilike("lastname", search + "%")),
                    Restrictions.ilike("email", search + "%")
            ));
        }
        if ("asc".equals(criteries.getOrderDirection())) {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).asc());
        } else {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).desc());
        }
//        get row count not filtered
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        criteria.setProjection(null);
        criteria.setFirstResult(criteries.getStart());
        criteria.setMaxResults(criteries.getLength());

        List<Users> users = criteria.list();
        session.close();

        return DataFormatter.getDataFeed(users, criteries.getDraw(), count.intValue());
    }

    public Users findOneByEmail(String email) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Users where email=:email");
        query.setParameter("email", email);
        Users user = (Users) query.uniqueResult();
        session.close();
        return user;
    }

    public Users findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Users where userid=:id");
        query.setParameter("id", id);
        Users user = (Users) query.uniqueResult();
        session.close();
        return user;
    }

    public List<Users> getUsers() {
        Session session = this.sessionFactory.openSession();
        List<Users> personList = session.createQuery("from Users ").list();
        session.close();
        return personList;
    }
}
