package platform.dao;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Examinations;
import platform.services.interfaces.IDataTables;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface ExaminationDAO extends IDataTables<Examinations> {

    Examinations findOne(Long id);

    Integer countLastMonth();

    DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries, Long userid, Long departmentid);
    void save(Examinations examinations);
}
