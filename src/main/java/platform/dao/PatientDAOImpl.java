package platform.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Patients;

import java.util.List;


/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Repository


public class PatientDAOImpl implements PatientDAO {

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Patients patient) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(patient);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(Patients patient) {
        Session session = this.sessionFactory.openSession();
        session.beginTransaction();
        session.saveOrUpdate(patient);
        session.getTransaction().commit();
        session.close();
    }

    public Patients findOne(Long id) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Patients where patientid=:id");
        query.setParameter("id", id);
        Patients patient = (Patients) query.uniqueResult();
        session.close();
        return patient;
    }

    public int getNum(){
        Session session = this.sessionFactory.openSession();
        return ((Long)session.createQuery("select count(*) from Patients where deleted = 0").uniqueResult()).intValue();
    }

    public Patients findOneByBirthNum(long birthnum) {
        Session session = this.sessionFactory.openSession();
        Query query = session.createQuery("from Patients where birthnum=:birthnum");
        query.setParameter("birthnum", birthnum);
        Patients patient = (Patients) query.uniqueResult();
        session.close();
        return patient;
    }

    public List<Patients> getPatients() {
        Session session = this.sessionFactory.openSession();
        List<Patients> personList = session.createQuery("from Patients where deleted=0").list();
        session.close();
        return personList;
    }

    @Transactional
    public DataTableFeed<Patients> findAllForCriteries(DataTableCriteries criteries) {
        Session session = this.sessionFactory.openSession();
        String search = criteries.getSearchFulltext();
        Criteria criteria = session.createCriteria(Patients.class);
        criteria.add(Restrictions.eq("deleted", 0));

        if (search != null && search != "") {
            criteria.add(Restrictions.or(
                    Restrictions.or(Restrictions.or(Restrictions.ilike("firstname", search + "%"),
                            Restrictions.ilike("lastname", search + "%")),
                            Restrictions.sqlRestriction("CAST(birthnum AS CHAR) like ?", search + "%", StandardBasicTypes.STRING)),
                    Restrictions.ilike("city", search + "%")))
            ;
        }
        if ("asc".equals(criteries.getOrderDirection())) {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).asc());
        } else {
            criteria.addOrder(Property.forName(criteries.getOrderColumn()).desc());
        }
//        get row count not filtered
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        criteria.setProjection(null);
        criteria.setFirstResult(criteries.getStart());
        criteria.setMaxResults(criteries.getLength());

        List<Patients> patients = criteria.list();
        session.close();

        return DataFormatter.getDataFeed(patients, criteries.getDraw(), count.intValue());
    }


}
