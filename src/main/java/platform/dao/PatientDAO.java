package platform.dao;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Patients;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface PatientDAO {
    void save(Patients user);
    void delete(Patients user);
    Patients findOne(Long id);
    DataTableFeed<Patients> findAllForCriteries(DataTableCriteries criteries);

    List<Patients> getPatients();
    Patients findOneByBirthNum(long birthnum);
    int getNum();

}
