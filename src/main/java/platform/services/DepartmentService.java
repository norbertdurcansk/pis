package platform.services;

import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.dao.DepartmentDAO;
import platform.models.Departments;
import platform.services.factories.ContextFactory;
import platform.services.interfaces.IDepartmentService;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
@Service
public class DepartmentService extends ContextFactory implements IDepartmentService {
    private DepartmentDAO departmentDAO = context.getBean(DepartmentDAO.class);

    public Departments findOne(Long id) {
        return departmentDAO.findOne(id);
    }

    public List<Departments> getList() {
        return departmentDAO.getList();
    }

    public DataTableFeed<Departments> findAllForDetail(DataTableCriteries criteries, Long id) {
        return departmentDAO.findAllForDetail(criteries, id);
    }

    public DataTableFeed<Departments> findAllForCriteries(DataTableCriteries criteries) {
        return departmentDAO.findAllForCriteries(criteries);
    }
    
    public void finalize(Departments department) throws Exception
    {
        if (department.getDepartmentid() == -1) {
            department.setDepartmentid(DataFormatter.getRandomUID());
        }
    }

    public Departments save(Departments user) {
        departmentDAO.save(user);
        return user;
    }
}
