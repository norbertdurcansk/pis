package platform.services;

import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.dao.DrugsDAO;
import platform.dao.PatientDrugDAO;
import platform.models.Drugs;
import platform.models.PatientDrug;
import platform.services.factories.ContextFactory;
import platform.services.interfaces.IDrugService;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Service
public class DrugService extends ContextFactory implements IDrugService {
    private DrugsDAO drugDAO = context.getBean(DrugsDAO.class);
    private PatientDrugDAO patientDrugDAO = context.getBean(PatientDrugDAO.class);

    public DataTableFeed<Drugs> findAllForCriteries(DataTableCriteries criteries) {
        return drugDAO.findAllForCriteries(criteries);
    }

    public DataTableFeed<Drugs> findAllForPatient(DataTableCriteries criteries, Long id) {
        return drugDAO.findAllForPatient(criteries, id);
    }

    public PatientDrug findOneDrug(Long id) {
        return patientDrugDAO.findOne(id);
    }

    public List<Drugs> getDrugs() {
        return drugDAO.getDrugs();
    }

    public void finalize(Drugs drug) throws Exception {
        if (drug.getDrugid() == -1) {
            drug.setDrugid(DataFormatter.getRandomUID());
        }
    }

    public void finalize(PatientDrug drug) {
        if (drug.getPatientdrugid() == -1) {
            drug.setPatientdrugid(DataFormatter.getRandomUID());
        }
    }

    public Drugs save(Drugs drug) {

        drugDAO.save(drug);

        return drug;
    }

    public PatientDrug save(PatientDrug drug) {

        patientDrugDAO.save(drug);

        return drug;
    }

    public Drugs findOne(Long id) {
        return drugDAO.findOne(id);
    }

    public List<Object> findDrugProducers() {
        return drugDAO.findDrugProducers();
    }
}
