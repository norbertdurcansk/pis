package platform.services;


import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.dao.UserDAO;
import platform.models.Users;
import platform.services.factories.ContextFactory;
import platform.services.interfaces.IUserService;

import java.util.List;


/**
 * Created by norbe on 3/7/2017.
 */

/**
 * Functions are defined in interface to be able to use them in controller
 * Each service extends ContextFactory class to get context for hibernate conf
 * Each Service can use specific daos for DB operations
 */
@Service
public class UserService extends ContextFactory implements IUserService {

    private UserDAO userDAO = context.getBean(UserDAO.class);

    public Users findOne(Long id) {
        return userDAO.findOne(id);
    }

    public void finalize(Users users, String password) throws Exception {
        if (password != null) {
            users.setPassword(DataFormatter.encryptPassword(password));
        }
        if (users.getUserid() == -1) {
            users.setUserid(DataFormatter.getRandomUID());
        }
    }

    public DataTableFeed<Users> findAllForCriteries(DataTableCriteries criteries) {
        return userDAO.findAllForCriteries(criteries);
    }

    public Users save(Users user) {
        userDAO.save(user);
        return user;
    }

    public List<Users> getUsers() {
        return userDAO.getUsers();
    }

    public Users create(Users user) {
        return null;
    }

    public Users findOneByEmail(String email) {
        return userDAO.findOneByEmail(email);
    }

//    public Users save(Users user){return userDAO.save(user);}

}
