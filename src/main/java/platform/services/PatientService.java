package platform.services;

import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.dao.PatientDAO;
import platform.models.Patients;
import platform.services.factories.ContextFactory;
import platform.services.interfaces.IPatientService;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Service
public class PatientService extends ContextFactory implements IPatientService {

    private PatientDAO patientDAO = context.getBean(PatientDAO.class);

    public Patients save(Patients patient) {

        patientDAO.save(patient);

        return patient;
    }

    public List<Patients> getPatients() {
        return patientDAO.getPatients();
    }

    public Patients delete(Patients patient) {

        patientDAO.delete(patient);

        return patient;
    }

    public Patients findOne(Long id){
        return patientDAO.findOne(id);
    }

    public DataTableFeed<Patients> findAllForCriteries(DataTableCriteries criteries) {
        return patientDAO.findAllForCriteries(criteries);
    }

    public void finalize(Patients patient) throws Exception {
        if (patient.getPatientid() == -1) {
            patient.setPatientid(DataFormatter.getRandomUID());
        }
    }

    public Patients findOneByBirthNum(long birthnum) {
        return patientDAO.findOneByBirthNum(birthnum);
    }

    public int getNumofPatients(){
        return patientDAO.getNum();
    }
}
