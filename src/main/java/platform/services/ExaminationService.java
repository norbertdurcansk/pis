package platform.services;

import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.controllers.BaseController;
import platform.dao.ExaminationDAO;
import platform.dao.UserDAO;
import platform.models.Examinations;
import platform.models.Users;
import platform.services.factories.ContextFactory;
import platform.services.interfaces.IExaminationService;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
@Service
public class ExaminationService extends ContextFactory implements IExaminationService {
    private ExaminationDAO examinationDAO = context.getBean(ExaminationDAO.class);
    private UserDAO userDAO = context.getBean(UserDAO.class);

    public void finalize(Examinations examinations) {
        if (examinations.getExaminationid() == -1L) {
            examinations.setExaminationid(DataFormatter.getRandomUID());
            examinations.setUserid(BaseController.getUserId());
            Users users = userDAO.findOne(BaseController.getUserId());
            examinations.setDepartmentid(users.getDepartmentid());
        }
    }

    public Examinations save(Examinations examinations) {
        examinationDAO.save(examinations);
        return examinations;
    }

    public Integer countLastMonth() {
        return examinationDAO.countLastMonth();
    }

    public DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries) {
        return null;
    }

    public DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries, Long userid, Long departmentid) {
        return examinationDAO.findAllForCriteries(criteries, userid, departmentid);
    }

    public Examinations findOne(Long id) {
        return examinationDAO.findOne(id);
    }
}
