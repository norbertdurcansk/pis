package platform.services.security;

//import org.hibernate.Session;
//import org.hibernate.query.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import platform.commons.DataFormatter;
import platform.models.Users;
import platform.services.interfaces.IUserService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//import platform.services.factories.SessionFactory;

/**
 * Created by norbe on 3/6/2017.
 */
@Service
public class WebSecurityService implements AuthenticationProvider,Serializable {

    @Autowired
    IUserService userService;


    /** ADD DATABASE SEARCH FOR USER AND SET ROLE */
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        Users user = userService.findOneByEmail(authentication.getName());
        if (user == null || user.getDeleted() == 1) {
            return null;
        }
        try{
            if (DataFormatter.encryptPassword(authentication.getCredentials().toString()).equals(user.getPassword())) {
                List<GrantedAuthority> auths=new ArrayList<GrantedAuthority>();
                auths.add(new SimpleGrantedAuthority("ROLE_"+user.getRole()));
                if (user.getRole().equals("ADMIN")) {
                    auths.add(new SimpleGrantedAuthority("ROLE_USER"));
                }
                auths.add(new SimpleGrantedAuthority("PERMISSION_"+user.getBitflags()));
                auths.add(new SimpleGrantedAuthority(String.valueOf(user.getUserid())));
                return new UsernamePasswordAuthenticationToken(authentication.getName(),authentication.getCredentials(),auths);
            }
        }catch (Exception e){}
        return null;
    }

    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
