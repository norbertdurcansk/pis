package platform.services.security;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;

import java.io.Serializable;

/**
 * Created by norbe on 3/7/2017.
 */
public class WebPermissionEvaluator implements PermissionEvaluator {


    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        String perm = authentication.getAuthorities().toArray()[authentication.getAuthorities().size() - 2].toString();
        if ((Integer.valueOf(perm.substring(perm.length() - 1)) & Integer.valueOf(permission.toString())) == Integer.valueOf(permission.toString())) {
            return true;
        }
        return false;
    }

    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        return false;
    }
}
