package platform.services.factories;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
public abstract class ContextFactory {
    public final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("hibernate.xml");
}
