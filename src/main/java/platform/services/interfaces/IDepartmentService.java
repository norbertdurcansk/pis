package platform.services.interfaces;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Departments;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/1/2017.
 */
public interface IDepartmentService extends IDataTables<Departments> {
    Departments findOne(Long id);

    List<Departments> getList();

    DataTableFeed<Departments> findAllForDetail(DataTableCriteries criteries, Long id);

    void finalize(Departments department) throws Exception;

    Departments save(Departments department);
}
