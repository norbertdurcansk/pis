package platform.services.interfaces;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Examinations;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface IExaminationService extends IDataTables<Examinations> {
    Examinations findOne(Long id);

    void finalize(Examinations examinations);

    Integer countLastMonth();
    DataTableFeed<Examinations> findAllForCriteries(DataTableCriteries criteries, Long userid, Long departmentid);

    Examinations save(Examinations examinations);
}
