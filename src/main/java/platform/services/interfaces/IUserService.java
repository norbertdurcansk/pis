package platform.services.interfaces;

//import platform.models.User;


import platform.models.Users;

import java.util.List;

/**
 * Created by norbe on 3/7/2017.
 */
public interface IUserService extends IDataTables<Users> {
    Users findOneByEmail(String email);

    Users findOne(Long id);

    void finalize(Users users, String password) throws Exception;

    Users save(Users user);

    List<Users> getUsers();
}
