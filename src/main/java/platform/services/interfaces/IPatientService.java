package platform.services.interfaces;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Patients;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface IPatientService {
    Patients findOne(Long id);

    void finalize(Patients patient) throws Exception;

    Patients findOneByBirthNum(long birthnum);

    //void finalize(Patients patient) throws Exception;
    List<Patients> getPatients();
    Patients save(Patients patient);
    Patients delete(Patients patient);
    DataTableFeed<Patients> findAllForCriteries(DataTableCriteries criteries);
    int getNumofPatients();
}
