package platform.services.interfaces;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;

/**
 * pis
 * Created by Norbert on 4/3/2017.
 */
public interface IDataTables<T> {
    public DataTableFeed<T> findAllForCriteries(DataTableCriteries criteries);
}
