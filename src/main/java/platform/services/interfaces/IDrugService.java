package platform.services.interfaces;

import platform.commons.DataTableCriteries;
import platform.commons.DataTableFeed;
import platform.models.Drugs;
import platform.models.PatientDrug;

import java.util.List;

/**
 * pis
 * Created by Norbert on 4/30/2017.
 */
public interface IDrugService {
    DataTableFeed<Drugs> findAllForCriteries(DataTableCriteries criteries);

    void finalize(Drugs drug) throws Exception;

    void finalize(PatientDrug drug);

    DataTableFeed<Drugs> findAllForPatient(DataTableCriteries criteries, Long id);

    PatientDrug findOneDrug(Long id);

    List<Drugs> getDrugs();
    Drugs save(Drugs drug);

    PatientDrug save(PatientDrug drug);
    Drugs findOne(Long id);

    List<Object> findDrugProducers();
}
