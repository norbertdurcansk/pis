<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
    <jsp:attribute name="head_container">
        <%--HEAD FOR THIS PAGE--%>
    </jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="javascript">var depid="${departmentid}";</script> <%-- Promenna vyexportovana z modelu pro tabulku --%>
        <script src="/resources/js/department-detail.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            <spring:message code="gn.departments"/>
        </h3>
        <div class="col-md-6">
            <h1 class="ui header">Detail oddělení: ${department_name}</h1>
            <table id="example" class=" table table-bordered dataTables_wrapper form-inline dt-bootstrap no-footer"
                   cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Jméno zaměstnance</th>
                    <th>Pozice</th>
                    <th>Email</th>
                </tr>
                </thead>
            </table>
        </div>
        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>