<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
    <jsp:attribute name="head_container">
        <%--HEAD FOR THIS PAGE--%>
    </jsp:attribute>
    <jsp:attribute name="body_container">
    <script src="/resources/js/department-new.js"></script>
        <div class="row">
                <div class="col-md-6">
                    <h2 class="ui header">Přidat nové oddělení:</h2>
                        <form id="addDepartment" action="/departments/add_department?id=${id_depart}" method="POST" enctype="application/json">
                            <div class="row" style="margin-top: 2em">
                                <div class="col-md-6">
                                    <div class="form-group required">
                                        <label>Název oddělení:</label>
                                        <input type="text"
                                               name="name"
                                               data-validation="required"
                                               data-validation-error-msg="Prosím vyplnit"
                                               class="form-control"
                                               placeholder="Název oddělení"
                                               value="${department.name}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" class="form-control"
                                           name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                    <button type="submit" class="btn btn-success" style="margin-bottom: 20px">Uložit
                                    </button>
                                </div>
                            </div>
                        </form>
                </div>
        </div>
    </jsp:attribute>

</l:layout>