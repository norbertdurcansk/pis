<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">

        <script src="/resources/js/setting.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            <spring:message code="gn.setting"/>
        </h3>
        <div class="row">
            <div class="col-md-12">
            <div class="ui  horizontal list">
                <sec:authorize access="hasPermission('PERMISSION','7')">
                <div class="item" onclick="window.location.href = '/setting/user/new'">
                    <div class=" ui icon button green">
                        Přidat
                        <i class="icon add user"></i>
                    </div>
                </div>
                </sec:authorize>
            </div>
            </div>
        </div>
            <div class="row">
                <sec:authorize access="hasPermission('PERMISSION','7')">
                <div class="col-md-6">
                </sec:authorize>
                <sec:authorize access="!hasPermission('PERMISSION','7')">
                <div class="col-md-6 col-md-offset-3">
                </sec:authorize>

                    <form id="userSetting" action="/setting/${user.userid}/update" method="POST" enctype="application/json">
                        <div class="row" style="margin-top: 2em">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Křestní jméno</label>
                                    <input type="text" name="firstname" data-validation="required" class="form-control"
                                           data-validation-error-msg="Prosím vyplnit"
                                           value="${user.firstname}" placeholder="Aa">
                                </div>

                                <div class="form-group">
                                    <label>Příjmení</label>
                                    <input type="text" name="lastname" data-validation="required" class="form-control"
                                           value="${user.lastname}"
                                           data-validation-error-msg="Prosím vyplnit"
                                           placeholder="Aa">
                                </div>

                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" data-validation="email" class="form-control"
                                           value="${user.email}"
                                           data-validation-error-msg="Prosím zadejte validní email"
                                           placeholder="Email">
                                </div>

                                <sec:authorize access="hasPermission('PERMISSION','7')">
                        <div class="form-group">
                            <label>Role</label>
                            <select class="form-control" name="role">
                                <option value="ADMIN" ${user.role=="ADMIN"? 'selected="selected"' : ''} >ADMIN</option>
                                <option value="USER" ${user.role=="USER"? 'selected="selected"' : ''} >USER</option>
                            </select>
                        </div>
                    </sec:authorize>

                                <div class="form-group">
                                    <label>Oddelení</label>

                                    <sec:authorize access="hasPermission('PERMISSION','7')">
                        <select class="form-control" name="departmentid">
                            <c:forEach items="${departments}" var="element">
                                     <option value="${element.departmentid}" ${element.departmentid == department.departmentid ? 'selected="selected"' : ''}>${element.name}</option>
                            </c:forEach>
                        </select>



                        </sec:authorize>

                                    <sec:authorize access="!hasPermission('PERMISSION','7')">
                            <span class="form-control">${department.name}</span>
                        </sec:authorize>

                                </div>

                            </div>
                            <div class="col-md-6">


                            <c:if test="${!newUser and user.userid==userid}">
                                <div class="form-group">
                                    <label>Heslo</label>
                                    <input type="password" name="oldpasswd"
                                           data-validation="custom" data-validation-regexp="^(.{5,})?$"
                                           data-validation-error-msg="Prosím vyplnit (min 5 znaků)"
                                           class="form-control">
                                </div>
                            </c:if>

                                <div class="form-group">
                                    <label>Nové heslo</label>
                                    <input type="password" name="newpasswd" class="form-control"
                                           data-validation="custom" data-validation-regexp="^(.{5,})?$"
                                           data-validation-error-msg="Hesla se musí shodovat (min 5 znaků)">
                                </div>
                                <div class="form-group">
                                    <label>Nové heslo</label>
                                    <input type="password" name="newpasswd1" class="form-control"
                                           data-validation="confirmation" data-validation-confirm="newpasswd"
                                           data-validation-error-msg="Hesla se musí shodovat (min 5 znaků)">
                                </div>

                                <sec:authorize access="hasPermission('PERMISSION','7')">
                        <div class="form-group">
                            <label>Práva</label>
                            <select class="form-control" name="bitflags">
                                <option value="1"  ${user.bitflags=="1"? 'selected="selected"' : ''}>Sestra</option>
                                <option value="3"  ${user.bitflags=="3"? 'selected="selected"' : ''}>Lékař</option>
                                <option value="7"  ${user.bitflags=="7"? 'selected="selected"' : ''}>Admin</option>
                            </select>
                        </div>
                    </sec:authorize>

                            </div>

                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="ui section divider"></div>

                                <input type="hidden" class="form-control"
                                       name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                <button type="submit" class="btn btn-success" style="margin-bottom: 20px">Uložit
                                </button>

                            </div>
                        </div>

                    </form>
                </div>

                <div class="col-md-6">
        <sec:authorize access="hasPermission('PERMISSION','7')">

                <table id="example" class=" table table-bordered dataTables_wrapper form-inline dt-bootstrap no-footer"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Jméno</th>
                        <th>Email</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>

        </sec:authorize>
                </div>
            </div>

        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>
