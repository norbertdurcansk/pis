<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>

<spring:url value="/resources/plugins/semantic-ui/semantic.css" var="semanticCSS"/>
<spring:url value="/resources/plugins/semantic-ui/semantic.js" var="semanticJS"/>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<link rel="icon" href="/resources/favicon.ico">
<link href="${semanticCSS}" rel="stylesheet"/>
<script src="${semanticJS}"></script>

<style type="text/css">
    body {
        background-color: white;
    }
    body > .grid {
        height: 100%;
    }
    .image {
        margin-top: -100px;
    }
    .column {
        max-width: 450px;
    }
</style>
<script>
    $(document)
        .ready(function() {
            $('.ui.form')
                .form({
                    fields: {
                        username: {
                            identifier  : 'username',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Prosim zadejte email'
                                },
                                {
                                    type   : 'email',
                                    prompt : 'Prosim zadejte validny e-mail'
                                }
                            ]
                        },
                        password: {
                            identifier  : 'password',
                            rules: [
                                {
                                    type   : 'empty',
                                    prompt : 'Prosim zadejte vase heslo'
                                },
                                {
                                    type   : 'length[5]',
                                    prompt : 'Vase heslo musi mat aspon 5 znakov'
                                }
                            ]
                        }
                    }
                })
            ;
        })
    ;
</script>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div>
    <div style="padding: 10% 10px 10px 10px;width: 50%;max-width: 450px;min-width: 290px;display: block;margin: 0 auto;">
        <h2 class="ui teal image header" style="width: 100%;text-align: center">
            <div class="content" style="color: #039BE5;">
                Přihlaste se do svého účtu
            </div>
        </h2>
        <form class="ui large form"  action='/j_spring_security_check' method='POST'>
            <div class="ui stacked">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="username" placeholder="email"
                               style="width:100%;display: block!important;">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Heslo"
                               style="width:100%;display: block!important;">
                    </div>
                </div>
                <input type="hidden"
                       name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <button type="submit" class="ui fluid large teal submit button" style="background-color: #039BE5">
                    Přihlásit
                </button>

            </div>
                <div class="ui error message"></div>
        </form>
    </div>
</div>

</body>
</html>