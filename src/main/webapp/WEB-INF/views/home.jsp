<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="JavaScript">
            var gdata = '${graphData}'
        </script>

        <script src="/resources/js/home.js"></script>
             <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
            <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
            <div class="row">
            <div class="col-md-8 col-md-offset-2">
            <div class="ui icon message" style="margin-bottom: 2em;">
                <i class="doctor icon"></i>
                <div class="content">
                    <div class="header">
                        Vítejte v nemocničním informačním systému!
                    </div>
                    <p>Aktuálně přihlášený uživatel: ${user.firstname} ${user.lastname}</p>
                </div>
            </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="col-md-6">
                    <div class="ui card text-center" style="margin: 0 auto;">
                        <div class="content">
                            <div class="header">Aktuální počet pacientů:</div>
                        </div>
                        <div class="content">
                            <div class="ui statistic">
                                <div class="value">
                                    ${count}
                                </div>
                                <div class="label">
                                    Pacientů
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ui card text-center" style="margin: 0 auto;">
                        <div class="content">
                            <div class="header">Vyšetření za posledních 30 dnů:</div>
                        </div>
                        <div class="content">
                            <div class="ui statistic">
                                <div class="value">
                                        ${examCount}
                                </div>
                                <div class="label">
                                    Vyšetření
                                </div>
                            </div>
                        </div>
                </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 100px;margin-left: -15px;">
            <div class="graph-container " style="padding: 30px;
                border: 1px solid #ccc;
                box-shadow: 1px 1px 2px #eee;
                position: relative;
                margin: 0 auto;
                width: 751px;">
                <div class="text-center"><h3>Počet léků dle dodavatele:</h3></div>
                <div id="producers" style="height: 255px;"></div>
            </div>


        </div>

        </div>

    </jsp:attribute>

</l:layout>


