<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">

        <script src="/resources/js/drugs.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            Přidat lék
        </h3>


            <div class="row">
                <div class="col-md-3"></div>

                <div class="col-md-6">
                    <form id="newDrug" action="/drugs/${drug.drugid}/update" method="POST"
                          enctype="application/json">
                        <div class="row" style="margin-top: 2em">
                            <div class="form-group required">
                                <label>Název</label>
                                <input type="text" name="name"
                                       data-validation="required"
                                       data-validation-error-msg="Prosím vyplnit"
                                       class="form-control" value="${drug.name}" placeholder="Ibalgin">
                            </div>

                            <div class="form-group">
                                <label>Typ</label>
                                <input type="text" name="type" class="form-control" value="${drug.type}" placeholder="Analgetikum">
                            </div>

                            <div class="form-group required">
                                <label>Aktivní látka</label>
                                <input type="text" name="activeSubstance" data-validation="required"
                                       data-validation-error-msg="Prosím vyplnit"
                                       class="form-control" value="${drug.activeSubstance}" placeholder="Brufen">
                            </div>

                            <div class="form-group required">
                                <label>Množství látky</label>
                                <input type="text"
                                       name="activeAmount"
                                       data-validation="required"
                                       data-validation-error-msg="Prosím vyplnit"
                                       data-validation="number"
                                       class="form-control"
                                       value="${drug.activeAmount}"
                                       placeholder="50 mg">
                            </div>

                            <div class="form-group">
                                <label>Výrobce</label>
                                <input type="text" name="producer" class="form-control" value="${drug.producer}" placeholder="Zentiva">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="ui section divider"></div>
                                <input type="hidden" class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                <button type="submit" class="btn btn-primary" style="margin-bottom: 20px">
                                    Uložit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>

        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>


