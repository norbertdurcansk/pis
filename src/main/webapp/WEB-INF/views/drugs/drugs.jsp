<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">

        <script src="/resources/js/drugs.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            Léky
        </h3>
        <div class="row">
            <div class="col-md-12">
                <div class="ui  horizontal list">
                    <sec:authorize access="hasPermission('PERMISSION','3')">
                        <a href="/drugs/create">
                            <div class="item">
                                <div class=" ui icon button green">
                                    Přidat
                                    <i class="icon add user"></i>
                                </div>
                            </div>
                        </a>
                    </sec:authorize>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1">


            </div>

            <div class="col-md-10">

                <sec:authorize access="hasPermission('PERMISSION','1')">
                    <table id="example" class=" table table-bordered dataTables_wrapper form-inline dt-bootstrap no-footer" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Název</th>
                            <th>Typ</th>
                            <th>Látka</th>
                            <th>Výrobce</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </sec:authorize>
            </div>

            <div class="col-md-1">


            </div>
        </div>


        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>
</l:layout>
