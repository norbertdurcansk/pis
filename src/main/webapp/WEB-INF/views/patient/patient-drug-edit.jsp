<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="JavaScript">
            var admin = false;
            var id =${patientdrug.patientid};
        </script>
        <script src="/resources/js/patient.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            Přidat lék
        </h3>

        <div class="row">

            <form id="updateDrug" action="/patient/drugs/update" method="POST"
                  enctype="application/json">
                <div class="row" style="margin-top: 2em">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-md-12">
                            <div class="form-group required">
                                <label>Léky</label>
                                <select class="form-control"
                                        name="drugid"
                                        data-validation="required">
                                <c:forEach items="${drugs}" var="element">
                                         <option value="${element.drugid}" ${element.drugid == patientdrug.drugid ? 'selected="selected"' : ''}>${element.name}</option>
                                </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group required">
                                <label>Počet tablet</label>
                                <input type="text" name="pillAmount" data-validation="required number"
                                       data-validation-error-msg="Prosím vyplnit číslo"
                                       class="form-control" value="${patientdrug.pillAmount}" placeholder="2">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group required">
                                <label>Denni množství</label>
                                <input type="text" name="dayAmount" data-validation=" required number"
                                       data-validation-error-msg="Prosím vyplnit číslo"
                                       class="form-control" value="${patientdrug.dayAmount}" placeholder="1">
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="patientid" value="${patientdrug.patientid}">
                    <input type="hidden" name="patientdrugid" value="${patientdrug.patientdrugid}">

                    <div class="col-md-6 col-md-offset-3">
                        <hr/>
                        <input type="hidden" class="form-control"
                               name="${_csrf.parameterName}" value="${_csrf.token}"/>

                        <button type="submit" class="btn btn-primary" style="margin-bottom: 20px">Uložit
                        </button>

                    </div>
                </div>


            </form>
                <%--//form --%>
        </div>

        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>
