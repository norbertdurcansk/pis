<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="JavaScript">
            var id = "";
        </script>
        <script src="/resources/js/patient.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            Pridať pacienta
        </h3>


            <div class="row">
                <div class="col-md-3"></div>

                <div class="col-md-6">
                    <form id="newPatient" action="/patient/${patient.patientid}/update" method="POST"
                          enctype="application/json">
                        <div class="row" style="margin-top: 2em">
                            <div class="col-md-6">

                                <div class="form-group required">
                                    <label>Rodní číslo</label>
                                    <input type="text"
                                           name="birthnum"
                                           data-validation="required length alphanumeric"
                                           data-validation-length="min9"
                                           data-validation-error-msg="Prosím vyplnit"
                                           class="form-control"
                                           value="${patient.birthnum}"
                                           placeholder="YYMMDDXXXX">
                                </div>

                                <div class="form-group required">
                                    <label>Křestní jméno</label>
                                    <input type="text"
                                           name="firstname"
                                           data-validation="required"
                                           data-validation-error-msg="Prosím vyplnit"
                                           class="form-control"
                                           value="${patient.firstname}"
                                           placeholder="Jan">
                                </div>

                                <div class="form-group required">
                                    <label>Příjmení</label>
                                    <input type="text"
                                           name="lastname"
                                           data-validation="required"
                                           data-validation-error-msg="Prosím vyplnit"
                                           class="form-control"
                                           value="${patient.lastname}"
                                           placeholder="Novák">
                                </div>

                                <div class="form-group required">
                                    <label>Datum narození</label>
                                    <input type="text" name="bday"
                                           data-validation="required"
                                           data-validation-error-msg="Prosím vyplnit"
                                           class="form-control"
                                           value="${bday}"
                                           placeholder="Datum narození">
                                    <input type="hidden" name="birthdate" value="${patient.birthdate}">
                                </div>


                                <div class="form-group required">
                                    <label>Pohlaví</label>
                                    <select class="form-control" name="sex">
                                        <option value="0" ${patient.sex=="0" ? 'selected="selected"' : ''}>  Muž  </option>
                                        <option value="1" ${patient.sex=="1" ? 'selected="selected"' : ''}>  Žena </option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group required">
                                    <label>Město</label>
                                    <input type="text" name="city"
                                           data-validation="required"
                                           data-validation-error-msg="Prosím vyplnit"
                                           class="form-control"
                                           value="${patient.city}"
                                           placeholder="Brno">
                                </div>

                                <div class="form-group">
                                    <label>Ulice</label>
                                    <input type="text" name="street" class="form-control" value="${patient.street}" placeholder="Masarykova 4">
                                </div>

                                <div class="form-group">
                                    <label>PSČ</label>
                                    <input type="text" name="postalcode" class="form-control"
                                           data-validation="custom"
                                           data-validation-regexp="^[0-9]*$"
                                           data-validation-error-msg="Prosím vyplnit validní PSČ"
                                           value="${patient.postalcode}" placeholder="60200">
                                </div>

                                <div class="form-group">
                                    <label>Telefon</label>
                                    <input type="text" name="phone" class="form-control"
                                           data-validation="custom"
                                           data-validation-regexp="^[0-9]*$"
                                           data-validation-error-msg="Prosím vyplnit validní telefon"
                                           value="${patient.phone}" placeholder="00420123456789">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="ui section divider"></div>
                                <input type="hidden" class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                                <button type="submit" class="btn btn-primary" style="margin-bottom: 20px">
                                    Uložit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3"></div>
            </div>


        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>


