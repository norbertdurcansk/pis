<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="JavaScript">
            var id = "${patient.patientid}";
            var isDoc = "${isDoc}";
        </script>

        <script src="/resources/js/patient.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            Náhled pacienta
        </h3>


            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="row" style="margin-top: 2em">
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Rodné číslo: </label>
                                ${patient.birthnum}
                            </div>

                            <div class="form-group">
                                <label>Křestní jméno: </label>
                                ${patient.firstname}
                            </div>

                            <div class="form-group">
                                <label>Příjmení: </label>
                                ${patient.lastname}
                            </div>

                            <div class="form-group">
                                <label>Datum narození: </label>
                                    ${bday}
                            </div>


                            <div class="form-group">
                                <label>Pohlaví: </label>
                                    ${patient.sex=="0" ? 'Muž' : 'Žena'}
                            </div>



                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label>Město: </label>
                                ${patient.city}
                            </div>

                            <div class="form-group">
                                <label>Ulice: </label>
                                ${patient.street}
                            </div>

                            <div class="form-group">
                                <label>PSČ: </label>
                                ${patient.postalcode}
                            </div>

                            <div class="form-group">
                                <label>Telefon: </label>
                                ${patient.phone}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

        <div class="row">
            <hr>

            <div class="col-md-8 col-md-offset-2">

                <div class="ui horizontal list">
                <sec:authorize access="hasPermission('PERMISSION','3')">
                <div class="item" onclick="window.location.href = '/patient/'+'${patient.patientid}'+'/drugs/-1/form'">
                    <div class=" ui icon button green">
                        Přidat
                        <i class="first aid icon"></i>
                    </div>
                </div>
                </sec:authorize>
                </div>
                <h1 class="ui header">Užívané léky:</h1>
                <sec:authorize access="hasPermission('PERMISSION','1')">
                    <table id="patientDrug"
                           class=" table table-bordered dataTables_wrapper form-inline dt-bootstrap no-footer"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Název</th>
                            <th>Druh</th>
                            <th>Účinná látka</th>
                            <th>Počet tablet</th>
                            <th>Denni množství</th>
                            <th>Výrobce</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </sec:authorize>

            </div>

        </div>
        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>


