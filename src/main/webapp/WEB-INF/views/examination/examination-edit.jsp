<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">
        <script language="JavaScript">
            var admin = false;
            var doctor = "";
        </script>
        <script src="/resources/js/examination.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            <spring:message code="gn.examinations"/>
        </h3>

        <div class="row">

            <form id="examForm" action="/examination/edit" method="POST"
                  enctype="application/json">
                <div class="row" style="margin-top: 2em">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-md-12">

                            <div class="form-group required">
                                <label>Pacient</label>
                                <select class="form-control" name="patientid"
                                        data-validation="required"
                                        data-validation-error-msg="Prosím vyplnit">
                                <c:forEach items="${patients}" var="element">
                                         <option value="${element.patientid}" ${element.patientid == examination.patientid ? 'selected="selected"' : ''}>
                                                 ${element.firstname} ${element.lastname}</option>
                                </c:forEach>
                                </select>
                            </div>
                            </div>
                        <div class="col-md-12">
                            <div class="form-group required" style="margin-top: 15px;">
                                <label>Popis</label>
                                <textarea maxlength="1000" class="form-control col-md-12" name="description"
                                          data-validation="required"
                                          data-validation-error-msg="Prosím vyplnit"
                                          style="max-width: 100%;margin-bottom: 15px;">${examination.description}</textarea>
                                <br/><br/>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group required">
                                <label>Datum</label>
                                <input type="text" class="form-control col-md-12" name="datestring" value="${date}"/>
                                <input type="hidden" class="form-control col-md-12" name="date"
                                       value="${examination.date}"/>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="examinationid" value="${examination.examinationid}">
                    <input type="hidden" name="userid" value="${examination.userid}">
                    <input type="hidden" name="departmentid" value="${examination.departmentid}">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>

                        <input type="hidden" class="form-control"
                               name="${_csrf.parameterName}" value="${_csrf.token}"/>

                        <button type="submit" class="btn btn-primary" style="margin-bottom: 20px">Uložit
                        </button>

                    </div>
                </div>


            </form>

                <%--//form --%>

        </div>

        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>
