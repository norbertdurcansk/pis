<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="l" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<l:layout>
<jsp:attribute name="head_container">
    <%--HEAD FOR THIS PAGE--%>
</jsp:attribute>
    <jsp:attribute name="body_container">

        <script language="JavaScript">
            var doctor = "${isdoc}";
            var admin = "${isadmin}";
        </script>
        <script src="/resources/js/examination.js"></script>

        <h3 class="ui horizontal divider header">
            <i class="settings icon"></i>
            <spring:message code="gn.examinations"/>
        </h3>
        <sec:authorize access="hasPermission('PERMISSION','3')and !hasPermission('PERMISSION','7')">
        <div class="row">
            <div class="col-md-12">
                <div class="ui  horizontal list">
                    <div class="item" onclick="window.location.href = '/examination/-1/form'">
                        <div class=" ui icon button green">
                            Přidat
                            <i class="heartbeat icon"></i>
                        </div>
                    </div>
                </div>
            </div>
            <br/><br/>
        </div>
        </sec:authorize>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <table id="example" class=" table table-bordered dataTables_wrapper form-inline dt-bootstrap no-footer"
                       cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center">Datum</th>
                        <th class="text-center">Jméno</th>
                        <th class="text-center">Rodné číslo</th>
                        <th class="text-center">Oddelení</th>
                        <th class="text-center">Lékař</th>
                        <th class="text-center"></th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <%--CONTENT FOR THIS PAGE --%>
    </jsp:attribute>

</l:layout>
