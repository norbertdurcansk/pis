<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix='c' uri='http://java.sun.com/jsp/jstl/core' %>
<%@ tag description="layout" pageEncoding="UTF-8" %>

<%@ attribute name="title" %>
<%@ attribute name="head_container" fragment="true" %>
<%@ attribute name="body_container" fragment="true" %>

<%--SEMANTIC--%>
<spring:url value="/resources/plugins/semantic-ui/semantic.css" var="semanticCSS"/>
<spring:url value="/resources/plugins/semantic-ui/semantic.js" var="semanticJS"/>
<%--CORE--%>
<spring:url value="/resources/js/init.js" var="initjs"/>
<spring:url value="/resources/css/init.css" var="initcss"/>
<%--BOOTSTRAP--%>
<spring:url value="/resources/plugins/bootstrap/css/bootstrap.css" var="bootcss1"/>
<spring:url value="/resources/plugins/bootstrap/js/bootstrap.js" var="bootjs"/>
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.3.0.js"></script>
<link href="${semanticCSS}" rel="stylesheet"/>
<link href="${bootcss1}" rel="stylesheet"/>
<script src="${bootjs}"></script>
<script src="${semanticJS}"></script>
<link href="${initcss}" rel="stylesheet"/>
<script src="${initjs}"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<%--<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet"/>--%>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

<%--daterange--%>
<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css"/>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>


<script src="/resources/plugins/notify/js/watnotif-1.0.min.js"></script>
<link href="/resources/plugins/notify/css/bubble-n-slide/watnotif.right-top-bubble.min.css" rel="stylesheet"/>
<link rel="icon" href="/resources/favicon.ico">
<%--form validation--%>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>

<html>
<head>
    <title><spring:message code="gn.title"/></title>
    <jsp:invoke fragment="head_container"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>

<div class="modal fade" id="delete-modal">
    <div class="modal-dialog" style="margin-top: 15%;">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #a94442;color: white">
                <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                <h4 class="modal-title">Pozor!</h4>
            </div>
            <div class="modal-body">
                <p>Opravdu si přejete smazat daný záznam?</p>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-save" onclick="window.location.href=redirect"
                        class="btn-ok btn btn-success" data-dismiss="modal">Ano
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Ne</button>
            </div>
        </div>

    </div>
</div>

<div class="ui left  visible vertical inverted sidebar labeled icon menu">
    <a class="item" id="home" href="/home">
        <i class="home icon"></i>
        <spring:message code="gn.home"/>
    </a>
    <a class="item" id="patients" href="/patient/home">
        <i class="users icon"></i>
        <spring:message code="gn.patient"/>
    </a>
    <a class="item" id="examination" href="/examination/home">
        <i class="heartbeat icon"></i>
        <spring:message code="gn.examinations"/>
    </a>

    <sec:authorize access="hasPermission('PERMISSION','3')">
    <a class="item" id="drugs" href="/drugs/home">
        <i class="first aid icon"></i>
        <spring:message code="gn.drugs"/>
    </a>
    </sec:authorize>

    <a class="item" id="departments" href="/departments/home">
        <i class="building icon"></i>
        <spring:message code="gn.departments"/>
    </a>
    <a class="item" id="settings" href="/setting/home">
        <i class="setting icon"></i>
        <spring:message code="gn.setting"/>
    </a>

    <a class="item" href="/logout">
        <i class="sign out icon"></i>
    </a>

</div>


<div class="ui segment spinner">
    <div class="ui active inverted dimmer">
        <div class="ui text loader"></div>
    </div>
    <p></p>
</div>

<div class="page">
<jsp:invoke fragment="body_container"/>
</div>

</body>
</html>