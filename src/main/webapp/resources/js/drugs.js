/**
 * Created by Jusko on 01-May-17.
 */

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif(data.error.toString(), "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/drugs/home";
        }, 1500);
    }
}


$(document).ready(function () {
    $('#drugs').addClass("active");

    $('#newDrug').submit(function (ev) {

        if (!$('#newDrug').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $.validate({
        form: '#newDrug',
        modules: 'security'
    });

    //Defining table user table
    $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/drugs/all/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'name',
                data: 'name',
                orderable: true
            }, { // 2
                name: 'type',
                data: 'type',
                orderable: true
            }, { // 3
                name: 'activeSubstance',
                data: 'activeSubstance',
                orderable: true
            }, { // 4
                name: 'producer',
                data: 'producer',
                orderable: true
            }, { // 5
                name: 'drugid',
                data: 'drugid',
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [2],
                "orderable": false,
                mRender: function (data, type, full) {
                    return full.activeSubstance + " " + full.activeAmount;
                }

            },
            {
                aTargets: [4],
                "orderable": false,
                mRender: function (data, type, full) {
                    var show = "";
                    var del = "";
                    show = '<a class="btn btn-info" style="width:90px; margin-right:5px" href="/drugs/edit?drugid=' + data + '"><i class="edit icon"></i><span>Upravit</span>';
                    del = '<a class="btn btn-danger" style="width:90px;margin-left: 5px;" ' +
                        'onclick="modal(\'/drugs/delete?drugid=' + data + '\')" data-toggle="modal" data-target="#delete-modal"><i class="trash outline icon"></i><span>Smazat</span>';
                    return show + del;
                }

            }
        ]


    });

});