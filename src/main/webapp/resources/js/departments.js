/**
 * Created by norbe on 4/1/2017.
 */

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif(data.error.toString(), "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/setting/home";
        }, 1500);
    }
}


$(document).ready(function () {
    $('#departments').addClass("active");

    $('#userSetting').submit(function (ev) {

        if (!$('#userSetting').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $.validate({
        form: '#userSetting',
        modules: 'security'
    });

    //Defining table user table
    $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/departments/home/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'name',
                data: 'name',
                orderable: true
            },
            { // 2
                name: 'buttons',
                data: 'buttons',
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [0],
                "orderable": false,
                mRender: function (data, type, full) {
                        return full.name;
                }
            },
            {
                aTargets: [1],
                "orderable": false,
                mRender: function (data, type, full) {
                    var button = '<a class="btn btn-default"  style="width:90px; margin-right: 5px" href="/departments/detail?id=' + full.departmentid.toString() + '"><i class="search icon"></i><span>Detail</span>';
                    if (isadm.toString() == "true")
                    {
                        button = button + '<a class="btn btn-info"  style="width:90px; margin-right: 5px" href="/departments/add?id=' + full.departmentid.toString() + '"><i class="edit icon"></i><span>Upravit</span>';
                        button = button + '<a class="btn btn-danger"  style="width:90px; margin-right: 5px"' +
                            ' onclick="modal(\'/departments/delete?id=' + full.departmentid.toString() + '\')"  data-toggle="modal" data-target="#delete-modal"><i class="trash outline icon"></i><span>Smazat</span>';
                    }
                    return button;
                }
            }
        ]
    });

});
