/**
 * Created by Jusko on 01-May-17.
 */

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif("Nastala chyba při editaci", "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/patient/home";
        }, 1500);
    }
}
function updateDrug(data) {

    if (data == "error" || data.error != null) {
        new Notif("Nastala chyba při editaci", "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/patient/show?patientid=" + id;
        }, 1500);
    }
}

$(document).ready(function () {
    $('#patients').addClass("active");

    $('#newPatient').submit(function (ev) {
        if (!$('#newPatient').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $('#updateDrug').submit(function (ev) {
        if (!$('#updateDrug').isValid())return;
        submitFormAjax(ev, $(this), updateDrug)
    });

    $.validate({
        form: '#newPatient',
        modules: 'security'
    });

    $.validate({
        form: '#updateDrug',
        modules: 'security'
    });

    $('#patientDrug').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/patient/" + id + "/drugs/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'name',
                data: 'name',
                orderable: true
            },
            { // 1
                name: 'type',
                data: 'type',
                orderable: true
            },
            { // 1
                name: 'activeSubstance',
                data: 'activeSubstance',
                orderable: true
            },
            { // 1
                name: 'pill_amount',
                data: 'pill_amount',
                orderable: true
            },
            { // 1
                name: 'day_amount',
                data: 'day_amount',
                orderable: true
            }, { // 1
                name: 'producer',
                data: 'producer',
                orderable: true
            },
            { // 1
                name: 'patientdrugid',
                data: 'patientdrugid',
                orderable: false,
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [6],
                "orderable": false,
                mRender: function (data, type, full) {
                    if (isDoc == "true") {
                        var del = '<a class="btn btn-danger" data-toggle="modal" data-target="#delete-modal" style="width:90px;margin-left: 5px;" onclick="modal(\'/patient/drugs/' + data + '/delete\')"' +
                            '><i class="trash outline icon"></i><span>Smazat</span>';
                        var update = '<a class="btn btn-info" style="width:90px;margin-left: 5px;" href="/patient/' + id + '/drugs/' + data + '/form"><i class="edit icon"></i><span>Upravit</span>';
                        return update + del;

                    } else return "-";
                }
            },
            {
                aTargets: [0],
                "orderable": false,
                mRender: function (data, type, full) {
                    if (isDoc == "true") {
                        return '<a href="/drugs/edit?drugid=' + full.drugid + '">' + data + '</a>';
                    } else return data;
                }
            }
        ]


    });


    //Defining table user table
    $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " "
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/patient/all/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'birthnum',
                data: 'birthnum',
                orderable: true
            }, { // 2
                name: 'lastname',
                data: 'lastname',
                orderable: true
            }, { // 3
                name: 'city',
                data: 'city'
            }, { // 4
                name: 'patientid',
                data: 'patientid',
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [1],
                "orderable": false,
                mRender: function (data, type, full) {
                    return full.firstname + " " + full.lastname;
                }

            },
            {
                aTargets: [3],
                "orderable": false,
                mRender: function (data, type, full) {
                    var button = "-";
                    var del = "";
                    var show = "";
                    show = '<a class="btn btn-default" style="width:90px; margin-right:5px" href="/patient/show?patientid=' + data + '"><i class="search icon"></i><span>Náhled</span>';
                    button = '<a class="btn btn-info" style="width:90px" href="/patient/edit?patientid=' + data + '"><i class="edit icon"></i><span>Upravit</span>';
                    del = '<a class="btn btn-danger" style="width:90px;margin-left: 5px;" data-toggle="modal" data-target="#delete-modal"' +
                        ' onclick="modal(\'/patient/delete?patientid=' + data + '\')"><i class="trash outline icon"></i><span>Smazat</span>';

                    return show + button + del;
                }

            }
        ]


    });

    $('input[name="bday"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Použit",
            "cancelLabel": "Zrušit",
            "fromLabel": "From"
        }
    }, function (start, end, label) {
        $('input[name="birthdate"]').val(start.format("YYYY-MM-DD HH:mm:ss"));

    })



});