/**
 * Created by norbe on 4/1/2017.
 */

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif(data.error.toString(), "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/setting/home";
        }, 1500);
    }
}


$(document).ready(function () {
    $('#settings').addClass("active");

    $('#userSetting').submit(function (ev) {

        if (!$('#userSetting').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $.validate({
        form: '#userSetting',
        modules: 'security'
    });

    //Defining table user table
    $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/setting/user/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'lastname',
                data: 'lastname',
                orderable: true
            }, { // 2
                name: 'email',
                data: 'email',
                orderable: true
            }, { // 2
                name: 'userid',
                data: 'userid',
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [0],
                "orderable": false,
                mRender: function (data, type, full) {
                    return full.firstname + " " + full.lastname;
                }

            },
            {
                aTargets: [2],
                "orderable": false,
                mRender: function (data, type, full) {
                    var button = '<a class="btn btn-info" style="width:90px;margin-left: 5px;" href="/setting/home?userid=' + data + '"><i class="edit icon"></i><span>Upravit</span>';
                    button += '<a class="btn btn-danger" data-toggle="modal" data-target="#delete-modal" style="width:90px;margin-left: 5px;"' +
                        'onclick="modal(\'/setting/' + data + '/delete\')" ><i class="trash outline icon"></i><span>Smazat</span>';
                    return button;
                }

            }
        ]


    });

});
