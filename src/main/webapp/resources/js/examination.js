/**
 * Created by norbe on 5/1/2017.
 */
/**
 * Created by norbe on 4/1/2017.
 */
var picker;

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif("Nastala chyba při editaci vyšetření", "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/examination/home";
        }, 1500);
    }
}

$(document).ready(function () {
    $('#examination').addClass("active");

    $('#examForm').submit(function (ev) {
        if (!$('#examForm').isValid())return;
        submitFormAjax(ev, $(this), update)
    });
    $.validate({
        form: '#examForm',
        modules: 'security'
    });
    //Defining table examination table
    var table = $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/examination/load",
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'date',
                data: 'date',
                orderable: true
            }, { // 2
                name: 'patient_lastname',
                data: 'patient_lastname',
                orderable: true

            }, { // 2
                name: 'birthnum',
                data: 'birthnum',
                orderable: true
            }, { // 2
                name: 'department_name',
                data: 'department_name',
                orderable: true
            },
            { // 2
                name: 'doctor_lastname',
                data: 'doctor_lastname',
                orderable: true
            },
            { // 2
                name: 'examinationid',
                data: 'examinationid',
                sClass: "text-center"
            }
        ],
        columnDefs: [
            {
                aTargets: [0],
                "orderable": false,
                mRender: function (data, type, full) {
                    var date = new Date(data);
                    return date.getDate() + "." + (date.getMonth() + 1) + "." + date.getFullYear() + " " + ("0" + date.getHours()).substr(-2) + ":" + ("0" + date.getMinutes()).substr(-2);
                }

            }, {
                aTargets: [1],
                "orderable": false,
                mRender: function (data, type, full) {
                    return '<a href="/patient/show?patientid=' + full.patientid + '">' + full.patient_firstname + ' ' + full.patient_lastname + '</a>';
                }
            }, {
                aTargets: [4],
                "orderable": false,
                mRender: function (data, type, full) {
                    if (admin == "true") {
                        return '<a href="/setting/home?userid=' + full.userid + '">' + full.doctor_firstname + ' ' + full.doctor_lastname + '</a>';
                    } else  return full.doctor_firstname + " " + full.doctor_lastname;
                }

            }, {
                aTargets: [3],
                "orderable": false,
                mRender: function (data, type, full) {
                    return '<a href="/departments/detail?id=' + full.departmentid + '">' + full.department_name + '</a>';
                }

            }, {
                aTargets: [5],
                "orderable": false,
                mRender: function (data, type, full) {
                    var button = "-";
                    var del = "";
                    if (doctor == "true") {
                        button = '<a class="btn btn-info" style="width:90px;margin-left: 5px;" href="/examination/' + data + '/form"><i class="edit icon"></i><span>Upravit</span>';
                        del = '<a class="btn btn-danger" style="width:90px;margin-left: 5px;" ' +
                            'onclick="modal(\'/examination/' + data + '/delete\')" data-toggle="modal" data-target="#delete-modal"><i class="trash outline icon"></i><span>Smazat</span>';
                    }
                    return button + del;
                }
            }
        ]


    });
    if (admin != "true") {
        table.column(3).visible(false);
        if (doctor == "true") {
            table.column(4).visible(false);
        }
    } else {
        table.column(3).visible(true);
        table.column(4).visible(true);
    }

    picker = $('input[name="datestring"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            "timePicker": true,
            "locale": {
                "format": "DD/MM/YYYY HH:mm:ss",
                "separator": " - ",
                "applyLabel": "Použit",
                "cancelLabel": "Zrušit",
                "fromLabel": "From"
            },
        },
        function (start, end, label) {
            $('input[name="date"]').val(start.format("YYYY-MM-DD HH:mm:ss"));
        });

});