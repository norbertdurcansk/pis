/**
 * Created by norbe on 3/2/2017.
 */

// plugin fucntion for datatables data adjusting
$.fn.planify = function (data) {
    for (var i = 0; i < data.columns.length; i++) {
        column = data.columns[i];
        column.searchRegex = column.search.regex;
        column.searchValue = column.search.value;
        delete (column.search);
    }
};

$(document).bind("ajaxSend", function () {
    $(".spinner").css("visibility", "visible")
}).bind("ajaxComplete", function () {
    $(".spinner").css("visibility", "hidden")
});

function submitFormAjax(ev, form, callback) {
    form.find('button[type=submit]').prop('disabled', true);
    $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: form.serialize(),
        success: function (data) {
            form.find('button[type=submit]').prop('disabled', false);
            callback(data);
        },
        error: function () {
            form.find('button[type=submit]').prop('disabled', false);
            callback("error");
        }
    });
    ev.preventDefault();
}
var redirect;

function modal(link) {
    redirect = link;
}
