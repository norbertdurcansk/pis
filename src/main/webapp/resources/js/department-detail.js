/**
 * Created by norbe on 4/1/2017.
 */

function update(data) {

    if (data == "error" || data.error != null) {
        new Notif(data.error.toString(), "error").display(1500);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(1500);
        setTimeout(function () {
            window.location.href = "/setting/home";
        }, 1500);
    }
}


$(document).ready(function () {
    $('#departments').addClass("active");

    $('#userSetting').submit(function (ev) {

        if (!$('#userSetting').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $.validate({
        form: '#userSetting',
        modules: 'security'
    });

    //Defining table user table
    $('#example').DataTable({

        "pagingType": "numbers",
        bAutoWidth: false,
        stateSave: true,
        bPaginate: true,
        bFilter: true,
        bLengthChange: true,
        searchHighlight: true,
        bInfo: false,
        language: {
            lengthMenu: "_MENU_",
            search: '<i class="search icon"></i> _INPUT_',
            decimal: ",",
            thousands: " ",
            emptyTable: "V tabulce se nenachází žádná data."
        },
        aLengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]],
        serverSide: true,
        ajax: {
            url: "/departments/detail/load?id=" + depid,
            data: function (data) {
                $.fn.planify(data);
            }
        },
        columns: [
            { // 1
                name: 'user_firstname',
                data: 'user_firstname',
                orderable: true
            },
            { // 2
                name: 'bitflags',
                name: 'bitflags',
                orderable: true
            },
            { //3
                name: 'name',
                orderable: true
            }
        ],
        columnDefs: [
            {
                aTargets: [0],
                "orderable": false,
                mRender: function (data, type, full) {
                    return full.user_firstname + " " + full.user_lastname;
                }
            },
            {
                aTargets: [1],
                "orderable": false,
                mRender: function (data, type, full) {
                    if (full.bitflags.toString() == '1')
                        return 'Sestra';
                    if (full.bitflags.toString() == '3')
                        return 'Lékař';
                    if (full.bitflags.toString() == '7')
                        return 'Administrátor';
                }
            },
            {
                aTargets: [2],
                "orderable": false,
                mRender: function (data, type, full) {
                    return full.user_email;
                }
            }
        ]
    });
});
