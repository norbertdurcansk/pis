/**
 * Created by norbe on 4/1/2017.
 */

function update(data) {
    if (data == "error" || data.error != null) {
        new Notif("Došlo k chybě při uložení do databáze.", "error").display(3000);
    } else {
        new Notif("Uložení proběhlo v pořádku!", "success").display(3000);
        setTimeout(function () {
            window.location.href = "/departments/home";
        }, 1500);
    }
}

$(document).ready(function () {
    $('#departments').addClass("active");

    $('#addDepartment').submit(function (ev) {

        if (!$('#addDepartment').isValid())return;
        submitFormAjax(ev, $(this), update)
    });

    $.validate({
        form: '#addDepartment',
        modules: 'security'
    });
});
